
//
//  SavedListViewController.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "SavedListViewController.h"
#import "protypeTableCell.h"
#import "RecordingListData.h"
#import "AppDelegate.h"
#import "RhythmClass.h"



#define NUM_TOP_ITEMS 20
#define NUM_SUBITEMS 6
#define IS_IPHONE_4s ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )480 ) < DBL_EPSILON )

@interface SavedListViewController () <MainNavigationViewControllerDelegate>
{
  RhythmClass *rhythmRecord;
}

@property (strong,nonatomic) SavedListDetailViewController *expander;
@property (nonatomic) CGRect chosenCellFrame;

@end
@implementation SavedListViewController

- (id)init {
    self = [super init];
    
    if (self) {
        
    }
    return self;
}

#pragma mark - View management

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
   
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //    songList = [[NSMutableArray alloc] initWithObjects:@"Tangos (100 BPM A#)",@"My Second Song",@"My Third Song",@"My Forth Song",@"Thriller Remix",@"MC Hammer"@"Save The World",@"Get Down",@"Get Up",@"Good Morning", nil];
    songList = [[NSMutableArray alloc] init];
    songList = [sqlManager getAllRecordingData];
   
    [_songTableView reloadData];
    NSLog(@"view will appear");
        if ([_selctRow isEqualToString:@"yes"]) {
            _selctRow = @"no";
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//            //[self.songTableView selectRowAtIndexPath:indexPath
//                                            animated:YES
//                                      scrollPosition:UITableViewScrollPositionNone];
            
                [self tableView:self.songTableView didSelectRowAtIndexPath:indexPath];
            
            
        }
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    sqlManager = [[DBManager alloc] init];
    
}

-(void)setDataToUIElements:(int)_index
{
    RecordingListData *cellData = [[RecordingListData alloc] init];
    rhythmRecord = [[RhythmClass alloc] init];
    
    cellData = [songList objectAtIndex:_index];
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    dataArray = [sqlManager fetchRhythmRecordsByID:[NSNumber numberWithInt:[cellData.rhythmID intValue]]];
    rhythmRecord = [dataArray objectAtIndex:0];
    
    currentRythmName = cellData.recordingName;
    songDuration = [NSString stringWithFormat:@"%@",[self timeFormatted:[cellData.durationString floatValue]]];
    dateOfRecording = cellData.dateString;
    songDetail = [NSString stringWithFormat:@"%@ %@ bpm %@",rhythmRecord.rhythmName,cellData.BPM,cellData.droneType];
    
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int second = totalSeconds % 60;
    int minute = (totalSeconds / 60) % 60;
    //int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d",minute, second];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return [songList count] + ((currentExpandedIndex > -1) ? [[subItems objectAtIndex:currentExpandedIndex] count] : 0);
    return [songList count];
    //return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *ParentCellIdentifier = @"songCell";
    //static NSString *ParentCellIdentifier = @"songCell";
    //static NSString *ChildCellIdentifier = @"childCell";
    
 
    //UITableViewCell *cell;
    
    
   protypeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:ParentCellIdentifier];
    
    if (cell == nil) {
        cell = [[protypeTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ParentCellIdentifier];
        //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    
        //cell.songNameLbl.text = [songList objectAtIndex:topIndex];
        [self setDataToUIElements:(int)indexPath.row];
        //cellData = [songList objectAtIndex:topIndex];
        cell.songNameLbl.text = currentRythmName;
        [cell.TotalTimeLbl setText:songDuration];
        cell.dateLbl.text = dateOfRecording;
        cell.songDetailLbl.text = songDetail;
           
    
    return cell;
    
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //cell.textLabel.backgroundColor = [UIColor clearColor];
    //cell.contentView.backgroundColor = [UIColor colorWithHue:.1 + .07*indexPath.row saturation:1 brightness:1 alpha:1];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (! self.expander) {
        if (IS_IPHONE_4s) {
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"4sStoryboard" bundle:[NSBundle mainBundle]];
            
            self.expander = [sb instantiateViewControllerWithIdentifier:@"Expander4s"];
        }
        else
        self.expander = [self.storyboard instantiateViewControllerWithIdentifier:@"Expander"];
        
        self.expander.delegate = self;
    }
    [self addChildViewController:self.expander];
    self.expander.view.frame = [self.songTableView rectForRowAtIndexPath:indexPath];
    self.expander.view.center = CGPointMake(self.expander.view.center.x, self.expander.view.center.y - self.songTableView.contentOffset.y); // adjusts for the offset of the cell when you select it
    self.chosenCellFrame = self.expander.view.frame;
    //self.expander.view.backgroundColor = [UIColor colorWithHue:.1 + .07*indexPath.row saturation:1 brightness:1 alpha:1];
    //UILabel *label = (UILabel *)[self.expander.cell viewWithTag:1];
    //label.text = self.theData[indexPath.row];
    [self.expander setDataForUIElements:(int)indexPath.row RecordingData:[songList objectAtIndex:indexPath.row]];
    [self.view addSubview:self.expander.view];
    [self.view bringSubviewToFront:self.expander.view];
    
    [UIView animateWithDuration:0.3 animations:^{
        //self.expander.view.frame = self.songTableView.frame;
        self.expander.view.frame = self.view.bounds;
        self.expander.collapseButton.alpha = 1;
    } completion:^(BOOL finished) {
        [self.bannerView removeFromSuperview];
        [self.admobBannerView removeFromSuperview];
        
        [self.expander didMoveToParentViewController:self];
    }];
    
}
-(void)expandedCellWillCollapse {
    [self.expander willMoveToParentViewController:nil];
    
        self.expander.view.frame = self.chosenCellFrame;
        self.expander.collapseButton.alpha = 0;
   
        [self.expander.view removeFromSuperview];
        [self.expander removeFromParentViewController];
        [songList removeAllObjects];
        songList = [sqlManager getAllRecordingData];
        RecordingListData *cellData = [[RecordingListData alloc] init];
    for (cellData in songList) {
        NSLog(@"T1 = %@",cellData.trackOne);
        NSLog(@"T2 = %@",cellData.trackTwo);
        NSLog(@"T3 = %@",cellData.trackThree);
        NSLog(@"T4 = %@",cellData.trackFour);
        
    }
    
        [_songTableView reloadData];
    
    if(bannerStatus) {
<<<<<<< HEAD
    [self.view addSubview:self.admobBannerView];
    [self.admobBannerView loadRequest:[GADRequest request]];
=======
//    [self.view addSubview:self.admobBannerView];
//    [self.admobBannerView loadRequest:[GADRequest request]];
>>>>>>> 5be2d5c177e7d99247aeecae86957aedb6cf421f
        //bannerStatus = NO;
    }
    else
    {
<<<<<<< HEAD
    [self.view addSubview:_bannerView];
=======
//    [self.view addSubview:_bannerView];
>>>>>>> 5be2d5c177e7d99247aeecae86957aedb6cf421f
    }
    
}



#pragma mark - IAd & Admob Delegate methods

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
    
    [self.bannerView removeFromSuperview];
    _admobBannerView = [[GADBannerView alloc]
                        initWithFrame:CGRectMake(0.0,20.0,
                                                 self.view.frame.size.width,
                                                 50)];
    
    // 3
    self.admobBannerView.adUnitID = @"ca-app-pub-4385871422548542/4655620110";
    self.admobBannerView.rootViewController = self;
    self.admobBannerView.delegate = self;
    NSLog(@"banner view addview");
    // 4
    [self.view addSubview:self.admobBannerView];
    [self.view bringSubviewToFront:self.expander.view];

    [self.admobBannerView loadRequest:[GADRequest request]];
    bannerStatus = YES;
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    [self.admobBannerView removeFromSuperview];
    [self.view addSubview:_bannerView];
    NSLog(@"banner view remove");
}
@end
