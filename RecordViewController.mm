//
//  RecordViewController.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "RecordViewController.h"
#import "FrequencyViewController.h"
#import <Foundation/Foundation.h>
#import "MainNavigationViewController.h"
#import "DBManager.h"
#import "RhythmClass.h"
#import "DroneName.h"

float tempo = 94.0f;
int caraouselIndex = 0;

@interface RecordViewController ()<MainNavigationViewControllerDelegate>{

    NSMutableArray *dronePickerArray, *countArray,*bpmPickerArray;
    BOOL clapFlag1, clapFlag2, clapFlag3, clapFlag4;
    int playFlag, stopFlag;
    int micCounter, endresult;
    
    // Sajiv Elements
    
    float sampleRate;
    NSString *duration;
    int seconds, minutes;
    int mCurrentScore, currentRhythmId;
    NSString *currentRythmName;
    
    NSError *audioSessionError;
    double peakPowerForChannel;
    NSString *currentMusicFileName;
    NSString *documentDir;
    NSString *newPath;
    AVAudioPlayer *myplayer;
    NSTimeInterval myTime;
    
    // Database related Tags or Flags
    NSString *musicDuration, *mergeFilePath;
    int inst1,inst2,inst3,inst4,vol1,vol2,vol3,vol4,bpm,timeStamp,t1,t2,t3,t4,t1Vol,t2Vol,t3Vol,t4Vol;
    
    NSMutableArray *rhythmArray, *droneArray;
    NSString *beatOneMusicFile, *beatTwoMusicFile;
    NSTimer *beatTimer;
    NSTimer *clap3Timer;
    
    int counter,grayCounter,redCounter;
    DropDown *dropDownObj;
}

@end

@implementation RecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    self.myNavigationController.delegate = self;
    
    [self.dropDownBgView setHidden:YES];
    
    // Do any additional setup after loading the view.
    _genreIdSelected = 1;
    bpmValue = 0;
    isStopped = 0;
    currentRhythmId = 0;
    
    [self resetFlags];
    
    mCurrentScore = 94;
    peakPowerForChannel = 0.0f;
    micCounter = endresult = 0;
    playFlag = stopFlag = seconds = minutes = 0;
    [_recordView setHidden:YES];
    
    [_playTimerBtn setHidden:YES];
    [_playStopBtn setHidden:YES];
    
    _myPort = nil;
    audioSessionError = nil;
    
    droneArray = [[NSMutableArray alloc]init];
    
    [_volumeSlider setMinimumTrackTintColor:[UIColor whiteColor]];
    [_volumeSlider setMaximumTrackTintColor:[UIColor blackColor]];
    [_volumeSlider setMaximumTrackImage:[UIImage imageNamed:@"slidertrackLatestBlack.png"] forState:UIControlStateNormal];
    [_volumeSlider setMinimumTrackImage:[UIImage imageNamed:@"sliderprogressLatest.png"] forState:UIControlStateNormal];
    [_volumeSlider setThumbImage:[UIImage imageNamed:@"slider-thumb.png"] forState:UIControlStateNormal];
    
    
    [_bpmSlider setThumbImage:[UIImage imageNamed:@"slider-thumb.png"] forState:UIControlStateNormal];
    [_bpmSlider setMinimumTrackTintColor:[UIColor whiteColor]];
    [_bpmSlider setMaximumTrackTintColor:[UIColor grayColor]];
    [_bpmSlider setMaximumTrackImage:[UIImage imageNamed:@"slidertrackLatest.png"] forState:UIControlStateNormal];
    [_bpmSlider setMinimumTrackImage:[UIImage imageNamed:@"sliderprogressLatest.png"] forState:UIControlStateNormal];
    
    [_dronePickerView setHidden:NO];
 //   [_dronePickerLayer setHidden:NO];
    _dronePickerView.layer.cornerRadius = 20.0;
    
    UITapGestureRecognizer *dismissPicker = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPicker:)];
    dismissPicker.delegate = self;
    
    [self.dronePickerView addGestureRecognizer:dismissPicker];
    
    UITapGestureRecognizer *clap3Tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clap3TapAction:)];
    clap3Tap.delegate = self;
    
    [self.bpmPickerView addGestureRecognizer:clap3Tap];
    
    UITapGestureRecognizer *dropDownTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dropDownLblTapAction)];
    dropDownTap.delegate = self;
    
    [self.genreBGView addGestureRecognizer:dropDownTap];
    
    UISwipeGestureRecognizer *swiperecognizer;
    swiperecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dropDownLblTapAction)];
    [swiperecognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    [self.genreBGView addGestureRecognizer:swiperecognizer];

    //for circular lay out of bpm pickerview
    self.bpmPickderBackView.layer.cornerRadius = 30;
    self.dronePickerBackView.layer.cornerRadius = 30;

    dronePickerArray = [[NSMutableArray alloc]init];
    bpmPickerArray = [[ NSMutableArray alloc]init];
    for (int i = 60; i <= 240 ; i++) {
        [bpmPickerArray addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    _carousel.type = iCarouselTypeCoverFlow2;
    
    // Scrollview hidden for timebeing
 //   [_topScrollView setHidden:YES];
    
    // Sajiv Elements
    _musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(handleVolumeChanged:)
                               name:MPMusicPlayerControllerVolumeDidChangeNotification
                             object:_musicPlayer];
    [_musicPlayer beginGeneratingPlaybackNotifications];
    
//    _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
//    [_bpmTxt setText:_bpmString];
//    [_bpmSlider setValue:mCurrentScore];
    
    [_volumeSlider setValue:_musicPlayer.volume * 10];
    
    _volumeView = [[MPVolumeView alloc] initWithFrame: CGRectMake(-100,-100,16,16)];
    _volumeView.showsRouteButton = NO;
    _volumeView.userInteractionEnabled = NO;
    [[[[UIApplication sharedApplication] windows] objectAtIndex:0] addSubview:_volumeView];
    
    
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    _recorder.meteringEnabled = YES;
    [_recorder prepareToRecord];
    
    _session = [AVAudioSession sharedInstance];
    [_session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    for (_input in [_session availableInputs]) {
        // set as an input the build-in microphone
        
        if ([_input.portType isEqualToString:AVAudioSessionPortBuiltInMic]) {
            _myPort = _input;
            break;
        }
    }
    
    [_session setPreferredInput:_myPort error:nil];
   // [_session setPreferredInput:_myPort error:&audioSessionError];
  //  [self setupApplicationAudio];
    [self registerForMediaPlayerNotifications];
    
    micArray = [[NSArray alloc]initWithObjects:mic1,mic2,mic3,mic4,mic5,mic6,mic7,mic8,mic9,mic10, nil];

    // Default values to be saved after app launches
    [self setDefaultValuesToMusicFiles];
    
    [self setUpDropDown];
    [self setUpBpmPicker];
    
    [_dropDownBtn setTitle:@"Metronome" forState:UIControlStateNormal];

    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:@"0",@"genreId",@"1",@"bpmDefault", nil];
    self.genreIdDict = dict;

    // showing Metronome by default
    [self setDropDownLblWithString:@"Metronome"];
 //   _droneType = @"A";
//    counter = 1,grayCounter = 1;
//    gap = 0.50;
//    
//    [NSTimer scheduledTimerWithTimeInterval:gap target:self selector:@selector(onTimer) userInfo:nil repeats:NO];
    [self fetchDBData];
    [_carousel reloadData];
    [_carousel scrollToItemAtIndex:carouselFirtValue duration:0.0f];
//    [dropDownObj reloadTableView];

    userDefaults = [NSUserDefaults standardUserDefaults];
    int value = [[userDefaults objectForKey:@"recodingid"] intValue];
    if (!value) {
        [userDefaults setObject:@"0" forKey:@"recodingid"];
        [userDefaults synchronize];
    }
    
    // Populating drone Values
    for (int i = 0; i < 50; i++) {
        [dronePickerArray addObjectsFromArray:droneArray];
    }
    _droneType = [droneArray objectAtIndex:0];
    [_dronePickerView reloadAllComponents];
    [_dronePickerView selectRow:300 inComponent:0 animated:NO];
}
-(void) onTimer {
    NSLog(@"this is timer action: %f",gap);
    gap = gap + .05;
    [NSTimer scheduledTimerWithTimeInterval:gap target:self selector:@selector(onTimer) userInfo:nil repeats:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 //   NSLog(@"view will appear");
//    [self setDropDownLblWithString:@"Metronome"];
    
    // By default micView has to appear
//    [_micView setHidden:YES];
    [_micView setHidden:NO];
    //  NSLog(@"RhythmArray = %@",[rhythmArray objectAtIndex:0]);

    if (_bpmDefaultFlag == 1) {
        NSLog(@"Index will appear ------ %d",(int)[_carousel currentItemIndex]);
        [self setDataToUIElements:(int)[_carousel currentItemIndex]];
    }
    CGRect visibleSize = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = visibleSize.size.width;
//    int xDist = ((screenWidth - 269) / 2);
//    
////    UIImageView *micGain1 = (UIImageView*)[self.micView viewWithTag:11];
////    micGain1.frame = CGRectMake(xDist, 36, 26, 3);
//    
//    _micGainFirstLeading.constant = xDist;
    //    _Instrument3_Layout.constant = ((screenWidth - 120) / 3);
    //    _Bpm_Layout.constant =  ((screenWidth - 120) / 3);
    //    _Intrument4_Layout.constant = ((screenWidth - 120) / 3);
    //    _drone_Layout.constant =  ((screenWidth - 120) / 3);
    //    _beat1Circle_Layout.constant = (((screenWidth) - 84)/5);
    //    _beat2Circle_Layout.constant = (((screenWidth) - 84)/5);
    //    _beat3Circle_Layout.constant = (((screenWidth) - 84)/5);
    //    _beat4Circle_Layout.constant = (((screenWidth) - 84)/5);
    //
    //    [self.view setNeedsUpdateConstraints];
    
    if (screenWidth == 414) {
        //
        //    _titleImageViewLayout.constant = 39;
        //    _carausalBgLayout.constant = 87;
        //    _carauselLayout.constant = 87;
        //    _blackView_Layout.constant = 249;
        //    _instrumentView_Layout.constant = 174;
        //    _beatsView_Layout.constant = 27;
        //    _micView_Layout.constant = 52;
        //    _recordView_Layout.constant = 335;
        [_dropDownLbl setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18.0]];
    }else if(screenWidth == 375){
        //        _titleImageViewLayout.constant = 35;
        //        _carausalBgLayout.constant = 79;
        //        _carauselLayout.constant = 79;
        //        _blackView_Layout.constant = 225;
        //        _instrumentView_Layout.constant = 157;
        //        _beatsView_Layout.constant = 25;
        //        _micView_Layout.constant = 47;
        //        _recordView_Layout.constant = 304;
        [_dropDownLbl setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:16.0]];
    }
    
    
  //  [self.beatsView setHidden:NO];
}

// Fetching data from Database for the First time
-(void)fetchDBData{

    [countArray removeAllObjects];
    _genreIdSelected = [[_genreIdDict valueForKey:@"genreId"] intValue];
    if (_genreIdSelected == 0) {
        _genreIdSelected = 1;
    }
    
    DBManager *rhythmQuery = [[DBManager alloc]init];
    rhythmArray = [rhythmQuery getRhythmRecords:[NSNumber numberWithInt:_genreIdSelected]];
    
    for (RhythmClass *cls in rhythmArray) {
        NSString *str = [cls valueForKey:@"rhythmName"];
        [countArray addObject:str];
    }
    NSMutableArray *emptyArray = [[NSMutableArray alloc]init];
    int numberOfTimes = 100;
    for (int i = 0; i < numberOfTimes; i++) {
        for (NSString *str in countArray) {
            [emptyArray addObject:str];
        }
    }
    carouselFirtValue = 49*(int)rhythmArray.count;
    
    countArray = emptyArray;
    RhythmClass *cls = [rhythmArray objectAtIndex:0];
    int startBpm = ( [cls.rhythmStartBPM  intValue] -60);
    [_bpmPickerView selectRow:startBpm inComponent:0 animated:NO];
    mCurrentScore = [cls.rhythmStartBPM  intValue];
 //   NSLog(@"the array obj is: %@",[bpmPickerArray objectAtIndex:startBpm]);
    currentRythmName = [countArray objectAtIndex:0];
  //  NSLog(@"carousel current value: %d",carouselFirtValue);
    
    // Fetch Drone Type
    NSArray *droneNames = [rhythmQuery getDroneName];
    for (DroneName *drone in droneNames) {
        [droneArray addObject:drone.droneName];
    }
}

-(void)setDefaultValuesToMusicFiles{
    
    inst1 = inst2 = 1;
    inst3 = inst4 = 0;
    vol1 = vol2 = vol3 = vol4 = 6;
    t1 = t2 = t3 = t4 = -1;
    t1Vol = t2Vol = t3Vol = t4Vol = -1;
    
    bpm = 60;
    timeStamp = 007;
    
    musicDuration = @"-1";
    mergeFilePath = @"-1";
}

- (void)awakeFromNib
{
    countArray = [[NSMutableArray alloc]init];
}

-(void)restoreValuesAfterScreenSlide{
    
}

- (float) bpmForSelectedRythm:(NSString*)_rythm{
    
    for (RhythmClass *cls in rhythmArray) {
        NSString *str = [cls valueForKey:@"rhythmName"];
        if ([str isEqualToString:_rythm]) {
            return [[cls valueForKey:@"rhythmBPM"] floatValue];
        }
        
    }
    return 0;
}

- (void)handleVolumeChanged:(id)notification {
    [_volumeSlider setValue:_musicPlayer.volume * 10];
}
#pragma mark - Timer Actions
-(void)changeRecBeatImages {
    
    if (counter == beatCount) {
        counter = 0;
    }
    if (redCounter == beatCount) {
        redCounter = 0;
    }
    UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:counter];
    [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];

    UIImageView *beatImg = (UIImageView*)[self.beatsView viewWithTag:redCounter];
    [beatImg setImage:[UIImage imageNamed:@"beat_ball_red.png"]];

    counter++;
    redCounter++;
}
-(void)onPlayTimer:(NSTimer *)timer {
    seconds++;
    if(seconds == 60)
    {
        seconds = 0;
        minutes++;
    }
    
    duration = [NSString stringWithFormat:@"%.2d:%.2d", minutes, seconds];
    // play timer change
    [_playTimerBtn setText:duration];
    
    // record timer value change
    [_recordTimerText setText:duration];
    
    [self setVolumeInputOutput];
}

- (void) updateMicGain:(NSTimer *)timer {
    [_recorder updateMeters];
    
    peakPowerForChannel = pow(10, (0.05 * ([_recorder peakPowerForChannel:0] + [_recorder averagePowerForChannel:0])/2));
    
    endresult = int(peakPowerForChannel*100.0f);
    
    if(endresult > 0 && endresult <= 9)
    {
        [self calculateMicGain:0];
    }
    else if(endresult > 9 && endresult <= 10)
    {
        [self calculateMicGain:1];
    }
    else if(endresult > 10 && endresult <= 100)
    {
        [self calculateMicGain:endresult/10 + 1];
    }
    
    
}

-(void)setVolumeInputOutput{
    currentOutputs = _session.currentRoute.outputs;
    for( _output in currentOutputs )
    {
        if([_output.portType isEqualToString:AVAudioSessionPortHeadphones])
        {
            [_session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
            [_session setPreferredInput:_myPort error:nil];
            break;
        }
        else if([_output.portType isEqualToString:AVAudioSessionPortBuiltInSpeaker])
        {
            break;
        }
        else if([_output.portType isEqualToString:AVAudioSessionPortBuiltInReceiver])
        {
            [_session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
            break;
        }
    }
}

- (void) calculateMicGain:(int) gain
{
    
    if(gain == 0)
    {
        for(micCounter = 0;micCounter < 10;micCounter++)
        {
            UIImageView *img = (UIImageView*)[micArray objectAtIndex:micCounter];
            [img setImage:[UIImage imageNamed:@"grey_strips.png"]];
            //micarray[micCounter]->setColor(Color3B(51, 51, 51));
        }
    }
    else if(gain > 0)
    {
        for(micCounter = 1;micCounter <= gain;micCounter++)
        {
            if(micCounter <= 7)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                [img setImage:[UIImage imageNamed:@"green-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::GREEN);
            }
            else if(micCounter == 8 || micCounter == 9)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                [img setImage:[UIImage imageNamed:@"orange-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::ORANGE);
            }
            else if(micCounter == 10)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                [img setImage:[UIImage imageNamed:@"red-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::RED);
            }
        }
        for(micCounter = gain+1;micCounter < 11;micCounter++)
        {
            UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
            [img setImage:[UIImage imageNamed:@"grey_strips.png"]];
            //micarray[micCounter-1]->setColor(Color3B(51, 51, 51));
        }
    }
}



-(void)resetFlags{
//    clapFlag3 = clapFlag4 = 0;
    clapFlag1 = clapFlag2 = 1;
}

-(void)viewDidLayoutSubviews{
    //[_topScrollView setContentSize:CGSizeMake(([countArray count] * 320), 80)];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
 //   NSLog(@"viewDidAppear");

}

- (UIStatusBarStyle)preferredStatusBarStyle{
   // return UIStatusBarStyleLightContent;
    
    return UIStatusBarStyleDefault;
}


-(void)viewDidDisappear:(BOOL)animated{
    //[super viewDidAppear:animated];
    
    [self resetFlags];
    
    if(_audioPlayer.isPlaying) {
        playFlag = 0;
        _audioPlayer.currentTime = 0;
        [_audioPlayer stop];
        [_playTimerBtn setHidden:YES];
        [_playStopBtn setHidden:YES];
        [_stopBtn setEnabled:YES];
        [_playBtn setBackgroundImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
    }
    
    if(_audioPlayerClap1.isPlaying) {
        _audioPlayerClap1.currentTime = 0;
        [_audioPlayerClap1 stop];
    }
    
    if(_audioPlayerClap2.isPlaying) {
        _audioPlayerClap2.currentTime = 0;
        [_audioPlayerClap2 stop];
    }
    
    if(_audioPlayerClap3.isPlaying) {
        _audioPlayerClap3.currentTime = 0;
        [_audioPlayerClap3 stop];
    }
    
    if(_audioPlayerClap4.isPlaying) {
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 stop];
    }
    playFlag = 1;
    UIButton *btn;
    [self onTapPlayBtn:btn];
    // first Stop timer
    [beatTimer invalidate];
    beatTimer = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleDronBtn {

//    NSLog(@"handle the single tap");
    UIButton *btn = (UIButton*)[self.view viewWithTag:44];
    [self onTapClap4Btn:btn];
    
}
- (void)clap3TapAction:(UITapGestureRecognizer*)gestureRecognizer {
    UIButton *btn = (UIButton *)[self.view viewWithTag:33];
    [self onTapClap3Btn:btn];
}

- (void)dismissPicker:(UITapGestureRecognizer*)gestureRecognizer {
    //    [_dronePickerLayer setHidden:YES];
    //    [_dronePickerView setHidden:YES];
    UIButton *btn = (UIButton*)[self.view viewWithTag:44];
    [self onTapClap4Btn:btn];
    NSLog(@"handle the single tap");
}

- (IBAction)onTapClap1Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag1 == 0) {
      //  _audioPlayerClap1.volume = 1;
        _audioPlayerClap1.volume = _musicPlayer.volume;
        [btn setSelected:YES];
        clapFlag1 = 1;
        if(playFlag == 1)
        {
//            [self playSelectedRhythms:currentRythmName];
        }
    } else {
        [btn setSelected:NO];
//        _audioPlayerClap1.currentTime = 0;
        _audioPlayerClap1.volume = 0;
//        [_audioPlayerClap1 stop];
        clapFlag1 = 0;
    }
    // [btn setImage:[UIImage imageNamed:@"Clap_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap2Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag2 == 0) {
        [btn setSelected:YES];
        _audioPlayerClap2.volume = _musicPlayer.volume;

        clapFlag2 = 1;
        if(playFlag == 1)
        {
//            [self playSelectedRhythms:currentRythmName];
        }
    } else {
        [btn setSelected:NO];
//        _audioPlayerClap2.currentTime = 0;
        _audioPlayerClap2.volume = 0;
//        [_audioPlayerClap2 stop];
        clapFlag2 = 0;
        
    }
    // [btn setImage:[UIImage imageNamed:@"Claps2_Selected.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap3Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag3 == 0) {
        [btn setSelected:YES];
        _audioPlayerClap3.volume = 1;
        clapFlag3 = 1;
        if(playFlag == 1)
        {
//            [self playSelectedRhythms:currentRythmName];
        }
    } else {
        [btn setSelected:NO];
        _audioPlayerClap3.volume = 0;
        clapFlag3 = 0;
//        _audioPlayerClap3.currentTime = 0;
//        [_audioPlayerClap3 stop];
    }
      [btn setImage:[UIImage imageNamed:@"Claps4_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap4Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag4 == 0) {
        [btn setSelected:YES];
        
        _audioPlayerClap4.volume = _musicPlayer.volume;
        clapFlag4 = 1;

        if(playFlag == 1)
        {
            [self playMemos:_droneType];
            _audioPlayerClap4.currentTime = 0;
            [_audioPlayerClap4 prepareToPlay];
            [_audioPlayerClap4 play];
        }
        
//        clapFlag4 = 1;
    } else {
        [btn setSelected:NO];
        _audioPlayerClap4.volume = 0;
        clapFlag4 = 0;
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 stop];
//        clapFlag4 = 0;
    }
    // [btn setImage:[UIImage imageNamed:@"Claps4_Blue.png"] forState:UIControlStateSelected];
}
#pragma mark - paly 4 players
-(void)play4Players {
    
    NSArray *listItems = [beatOneMusicFile componentsSeparatedByString:@"/"];
    NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];

    _audioPlayerClap1 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString]] error:nil];
    //[self playRhythmsWithBeats:_audioPlayerClap1 Beat:0];
    

    if (clapFlag1 == 0) {
        _audioPlayerClap1.volume = 0;
    }
    else {
      //  _audioPlayerClap1.volume = 1;
        _audioPlayerClap1.volume = 1;
    }
    
    [_audioPlayerClap1 prepareToPlay];
    _audioPlayerClap1.numberOfLoops = -1;
    _audioPlayerClap1.enableRate = true;
    _audioPlayerClap1.rate = mCurrentScore/tempo;
    
    
    listItems = [beatTwoMusicFile componentsSeparatedByString:@"/"];
    lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];
    NSLog(@"Player 1 ");
    _audioPlayerClap2 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString ]] error:nil];
    if (clapFlag2 == 0) {
        _audioPlayerClap2.volume = 0;
    }
    else {
   //     _audioPlayerClap2.volume = 1;
        _audioPlayerClap2.volume = 1;
    }
    //[self playRhythmsWithBeats:_audioPlayerClap2 Beat:0];

    [_audioPlayerClap2 prepareToPlay];
    _audioPlayerClap2.numberOfLoops = -1;
    _audioPlayerClap2.enableRate = true;
    _audioPlayerClap2.rate = mCurrentScore/tempo;
    
    _audioPlayerClap3 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Min_Click_60bpm.m4a", [[NSBundle mainBundle] resourcePath]]] error:nil];
  //  float beatFrequency = 60.0 / mCurrentScore;
    if (clapFlag3 == 0) {
        _audioPlayerClap3.volume = 0;
    }
    else {
        //_audioPlayerClap3.volume = 1;
        _audioPlayerClap3.volume = 1;
    }
    
     if (clapFlag3 == 1) {
     _audioPlayerClap3.rate = mCurrentScore/60.0;
     _audioPlayerClap3.enableRate = true;
    _audioPlayerClap3.numberOfLoops = -1;
    //_audioPlayerClap3.rate = mCurrentScore/tempo;
     [_audioPlayerClap3 prepareToPlay];
     
     
     }
    
    if(clapFlag4 == 1) {
        [self playMemos:_droneType];
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 prepareToPlay];
        [_audioPlayerClap4 play];
    }
    
    
//    if (!clap3Timer) {
//        clap3Timer = [NSTimer scheduledTimerWithTimeInterval:beatFrequency target:self selector:@selector(clap3timerMethod) userInfo:nil repeats:YES];
//        [[NSRunLoop mainRunLoop] addTimer:clap3Timer forMode:NSRunLoopCommonModes];
//    }
    
    myTime = _audioPlayerClap3.deviceCurrentTime;
    myTime = myTime + 0.2;
    
    [_audioPlayerClap1 playAtTime:myTime];
    [_audioPlayerClap2 playAtTime:myTime];
    [_audioPlayerClap3 playAtTime:myTime];
    
   // [self playRhythmsWithBeats:_audioPlayerClap2 Beat:0];
}

-(void)clap3timerMethod {
    if (clapFlag3 == 1 &&( stopFlag ==1 || playFlag == 1)) {
        [_audioPlayerClap3 prepareToPlay];
        [_audioPlayerClap3 play];
    }
}

- (void)resetAllPlayers {
    _audioPlayerClap1 = nil;
    _audioPlayerClap2 = nil;
    _audioPlayerClap3 = nil;
    _audioPlayerClap4 = nil;
    
}

- (IBAction)onTapPlayBtn:(id)sender {
    // Show 2 Labels
    
    // Play button selected and audio is playing
    if (playFlag == 0) {
        [_stopBtn setEnabled:NO];
        playFlag = 1;

        [_playBtn setBackgroundImage:[UIImage imageNamed:@"stopicon@2x.png"] forState:UIControlStateNormal];
        [_playTimerBtn setHidden:NO];
        [_playStopBtn setHidden:NO];
        
      //  [self playSelectedRhythms:currentRythmName];
        [self play4Players];
        
//        [self playRhythmsWithBeats:_audioPlayerClap3 Beat:0];

        
        _playTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f
                                                      target: self
                                                    selector:@selector(onPlayTimer:)
                                                    userInfo: nil repeats:YES];
        [_playTimer fire];
        
        
        // Call BeatMeter Image change method
//        counter = 0;
        counter = 1;
//        grayCounter = beatCount-1;
        grayCounter = beatCount;
//        if (clapFlag3 == 1) {
//            _audioPlayerClap3.rate = mCurrentScore/60.0;
//            _audioPlayerClap3.enableRate = true;
//           // _audioPlayerClap3.rate = mCurrentScore/tempo;
//            [_audioPlayerClap3 prepareToPlay];
//            [_audioPlayerClap3 playAtTime:myTime];
//
//        }
        for (int i = 0; i < 12; i++) {
            UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:i];
            if (i == 0) {
                [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_green.png"]];
            }
            else
            [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
        }
        [self changeBeatMeterImages];
        
    }
    else if(playFlag == 1){
        
        [self enableDropDownLbl:YES];

        [_stopBtn setEnabled:YES];
        [_playBtn setBackgroundImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
        
        [_playTimerBtn setHidden:YES];
        [_playStopBtn setHidden:YES];
        
        [self stopSelectedRhythms];
        
        [self resetAllPlayers];
        [_recorder stop];

        [_playTimer invalidate];
        _playTimer = nil;
        
        [_recordTimer invalidate];
        _recordTimer = nil;
        
        [beatTimer invalidate];
        beatTimer = nil;
        
        [_recImgTimer invalidate];
        _recImgTimer = nil;
        
        _recordTimerText.text = @"00:00";
        [self.recordDelegate recordingDone];
        
        for (int i = 0; i < 12; i++) {
            UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:i];
            [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
        }
        
        seconds = 0;
        minutes = 0;
        
        playFlag = 0;
    }
    
    if (stopFlag == 1) {
        [_playBtn setSelected:NO];
        [_stopBtn setSelected:NO];
        stopFlag = 0;
        playFlag = 0;
        
        _recordTimerText.text = @"00:00";
        [self calculateMicGain:0];
        [_recordView setHidden:YES];
        [_bpmView setHidden:NO];
        [_micView setHidden:NO];
    }
}

- (IBAction)onTapStopBtn:(id)sender {
//    [self enableDropDownLbl:YES];
    // change 2 buttons image
    
    // Recording button pressed
    if (stopFlag == 0) {
        [_playBtn setSelected:YES];
        [_stopBtn setSelected:YES];
        
        [self enableDropDownLbl:NO];

        stopFlag = 1;
        playFlag = 1;
        
        [_recordView setHidden:NO];
        [_bpmView setHidden:YES];
        [_micView setHidden:NO];
        [self.recordDelegate tappedRecordButton];
        
//        [self.tunerBtn setUserInteractionEnabled:NO];
//        [self.tunerBlackImage setHidden:NO];
        
      //  [self playSelectedRhythms:currentRythmName];
        [self play4Players];
        
        [_recorder record];
        
        _playTimer = [NSTimer scheduledTimerWithTimeInterval: 0.01f
                                                      target: self
                                                    selector:@selector(updateMicGain:)
                                                    userInfo: nil repeats:YES];
        [_playTimer fire];
        
        _recordTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f
                                                      target: self
                                                    selector:@selector(onPlayTimer:)
                                                    userInfo: nil repeats:YES];
        [_recordTimer fire];

//        [self changeBeatMeterImages];
        redCounter = 1;
        counter = 0;
        float beatFrequency = 60.0 / mCurrentScore;

        if ([_recImgTimer isValid]) {
            [_recImgTimer invalidate];
        }
        _recImgTimer = nil;
//        NSLog(@"recording timer action: %f & %@",beatFrequency,_recImgTimer);

        UIImageView *beatImg = (UIImageView*)[self.beatsView viewWithTag:0];
        [beatImg setImage:[UIImage imageNamed:@"beat_ball_red.png"]];
        if (!_recImgTimer) {
            _recImgTimer = [NSTimer scheduledTimerWithTimeInterval:beatFrequency target:self selector:@selector(changeRecBeatImages) userInfo:nil repeats:YES];
        }
        
//        [recImgTimer fire];
    } // End of recording button
    else if (stopFlag == 1) {
        [_playBtn setSelected:NO];
        [_stopBtn setSelected:NO];
        

        [_recImgTimer invalidate];
        _recImgTimer = nil;

        UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:redCounter-1];
        [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];

        [_recorder stop];
        
        [self.recordDelegate recordingDone];
        
        [_playTimer invalidate];
        _playTimer = nil;
        
        // to reset record timer text
        [_recordTimer invalidate];
        _recordTimer = nil;
        
        seconds = 0;
        minutes = 0;
        
        [self stopSelectedRhythms];
        [self resetAllPlayers];
        [self calculateMicGain:0];   // to reset mic gain images to grey
        
        [self saveRhythmRecording];
        
        stopFlag = 0;
        playFlag = 0;

        _recordTimerText.text = @"00:00";
        [_recordView setHidden:YES];
        [_bpmView setHidden:NO];
        [_micView setHidden:NO];
    }
}

- (IBAction)onTapPlusBtn:(id)sender {
    if (mCurrentScore < 240) {
        
        mCurrentScore++;
        sampleRate = mCurrentScore;
        
        if(_audioPlayer.isPlaying) {
            _audioPlayer.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap1.isPlaying) {
            _audioPlayerClap1.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap2.isPlaying) {
            _audioPlayerClap2.rate = sampleRate/tempo;
        }
        if(_audioPlayerClap3.isPlaying) {
            _audioPlayerClap3.rate = sampleRate/60.0;
        }
        
        if(_audioPlayerClap4.isPlaying) {
            _audioPlayerClap4.rate = sampleRate;
        }
        
        _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
        [_bpmTxt setText:_bpmString];
        [_bpmSlider setValue:sampleRate];
        bpmValue = mCurrentScore;
        
        // Call BeatMeter Image change method
        // first Stop timer
        [beatTimer invalidate];
        beatTimer = nil;
        
        if (playFlag == 1) {
            [self changeBeatMeterImages];
        }
    }
}

- (IBAction)onTapMinusBtn:(id)sender {
    if (mCurrentScore > 60) {
        mCurrentScore--;
        
        sampleRate = mCurrentScore;
        
        if(_audioPlayer.isPlaying) {
            _audioPlayer.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap1.isPlaying) {
            _audioPlayerClap1.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap2.isPlaying) {
            _audioPlayerClap2.rate = sampleRate/tempo;
        }
        if(_audioPlayerClap3.isPlaying) {
            _audioPlayerClap3.rate = sampleRate/60.0;
        }
        
        if(_audioPlayerClap4.isPlaying) {
            _audioPlayerClap4.rate = sampleRate/tempo;
        }
        
        _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
        [_bpmTxt setText:_bpmString];
        [_bpmSlider setValue:sampleRate];
        bpmValue = mCurrentScore;
        
        // Call BeatMeter Image change method
        // first Stop timer
        [beatTimer invalidate];
        beatTimer = nil;
        
        if (playFlag == 1) {
            [self changeBeatMeterImages];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"button Index =  %ld",(long)buttonIndex);
    if (buttonIndex == 1) {
        currentMusicFileName = [alertView textFieldAtIndex:0].text;

        [self saveRhythmRecording];
    }
}
- (NSString *) timeStamp {
    return [NSString stringWithFormat:@"%ld",(long int)[[NSDate date] timeIntervalSince1970]];
}
- (void)saveRhythmRecording {
    // Get the input text
    [self enableDropDownLbl:YES];
//    currentMusicFileName = [alertView textFieldAtIndex:0].text;
    int value = [[userDefaults objectForKey:@"recodingid"] intValue];

    currentMusicFileName = [NSString stringWithFormat:@"Recording %d",++value];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d",value] forKey:@"recodingid"];
    [userDefaults synchronize];

    //tempoCounter = NSString(audioPlayer.rate;
    time_t     now = time(0);
    struct tm  tstruct;
    char       date[80];
    char       time[80];
    tstruct = *localtime(&now);
    strftime(date, sizeof(date), "%d/%m/%Y", &tstruct);
    strftime(time, sizeof(time), "%X", &tstruct);
    
   // currentMusicFileName=[currentMusicFileName stringByAppendingString:@".m4a"];
    NSString *str = [NSString stringWithFormat:@"Recording %d",value];
    [self renameFileName:@"MyAudioMemo.m4a" withNewName:str];

    
    AVAudioPlayer * sound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:newPath] error:nil];
    NSLog(@"Duration = %f",sound.duration);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"MM/dd/yyyy"];
    
    NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat: @"HH:mm:ss"];
    
    NSString *stringFromTime = [timeFormatter stringFromDate:[NSDate date]];
    
    if(_droneType == nil)
        _droneType = @"-1";
    // Have to change BPM value instaed of _bpmTxt.text in next line
    
    NSDictionary *musicDict = [[NSDictionary alloc]initWithObjectsAndKeys:currentMusicFileName,@"name",[NSString stringWithFormat:@"%d",(clapFlag1)],@"inst1",[NSString stringWithFormat:@"%d",(clapFlag2)],@"inst2",[NSString stringWithFormat:@"%d",(clapFlag3)],@"inst3",[NSString stringWithFormat:@"%d",(clapFlag4)],@"inst4",[NSString stringWithFormat:@"%d",vol1],@"vol1",[NSString stringWithFormat:@"%d",vol2],@"vol2",[NSString stringWithFormat:@"%d",vol3],@"vol3",[NSString stringWithFormat:@"%d",vol4],@"vol4",[NSString stringWithFormat:@"%d",currentRhythmId],@"rhythmId",[NSString stringWithFormat:@"%d",mCurrentScore],@"bpm",stringFromDate,@"date",stringFromTime,@"time",[NSString stringWithFormat:@"%f",sound.duration], @"duration",_droneType,@"droneType",newPath,@"t1",@"-1",@"t2",@"-1",@"t3",@"-1",@"t4",@"6",@"t1vol",@"-1",@"t2vol",@"-1",@"t3vol",@"-1",@"t4vol",@"-1",@"mergefile",@"0",@"isDeleted",[NSString stringWithFormat:@"%f",sound.duration],@"t1Duration",@"-1",@"t2Duration",@"-1",@"t3Duration",@"-1",@"t4Duration", nil];
    
    NSLog(@"dict = %@",musicDict);
    
    DBManager *saveRhythm = [[DBManager alloc]init];
    [saveRhythm insertDataToRecordingInDictionary:musicDict];
    
    [_myNavigationController viewToPresent:1 withDictionary:musicDict];

}
-(BOOL)renameFileName:(NSString*)oldname withNewName:(NSString*)newname{
    documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                       NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *oldPath = [documentDir stringByAppendingPathComponent:oldname];
    newPath = [documentDir stringByAppendingPathComponent:newname];
    
    
    
    
    
    NSFileManager *fileMan = [NSFileManager defaultManager];
    NSError *error = nil;
    
    if (![fileMan moveItemAtPath:oldPath toPath:newPath error:&error])
    {
        NSLog(@"Failed to move '%@' to '%@': %@", oldPath, newPath, [error localizedDescription]);
        return false;
    }
    
    return true;
}

- (IBAction)OnChangeVolumeSlider:(id)sender {
    _musicPlayer.volume = [_volumeSlider value]/10.0f;
}

- (IBAction)onChangeBPM:(id)sender {
    
//    mCurrentScore = [_bpmSlider value];
    NSLog(@"mCurrentScore = %d",mCurrentScore);
    
    if(_audioPlayer.isPlaying) {
        _audioPlayer.rate = mCurrentScore/tempo;
    }
    
    if(_audioPlayerClap1.isPlaying) {
        _audioPlayerClap1.rate = mCurrentScore/tempo;
    }
    
    if(_audioPlayerClap2.isPlaying) {
        _audioPlayerClap2.rate = mCurrentScore/tempo;
    }
    if(_audioPlayerClap3.isPlaying) {
        _audioPlayerClap3.rate = mCurrentScore/60.0;
    }
//    mCurrentScore = [_bpmSlider value];
    _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
    [_bpmTxt setText:_bpmString];
    bpmValue = mCurrentScore;
    
    // Call BeatMeter Image change method
        // first Stop timer
        [beatTimer invalidate];
        beatTimer = nil;
    if (playFlag == 1) {
        [self changeBeatMeterImages];
    }
}

-(void)changeBeatMeterImages{
    
    // Take Beat meter count and current BPM
    float beatFrequency = 60.0 / mCurrentScore;
    NSLog(@"beatFreq = %f",beatFrequency);
    
    if ([beatTimer isValid]) {
        [beatTimer invalidate];
       // beatTimer = nil;
    }
    if ([clap3Timer isValid]) {
        [clap3Timer invalidate];
    }
    beatTimer = [NSTimer scheduledTimerWithTimeInterval:beatFrequency target:self selector:@selector(changeBeatMeterImage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:beatTimer forMode:NSRunLoopCommonModes];

    
//    clap3Timer = [NSTimer scheduledTimerWithTimeInterval:beatFrequency target:self selector:@selector(clap3timerMethod) userInfo:nil repeats:YES];
//    [[NSRunLoop mainRunLoop] addTimer:clap3Timer forMode:NSRunLoopCommonModes];
    
}

-(void)changeBeatMeterImage{
    
    if(beatCount == counter)
    {
        counter = 0;
    }
    if (grayCounter == beatCount) {
        grayCounter = 0;
    }
    
    UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:grayCounter];
    [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];

        UIImageView *beatImg = (UIImageView*)[self.beatsView viewWithTag:counter];
        [beatImg setImage:[UIImage imageNamed:@"beat_ball_green.png"]];

    
    counter++;
    grayCounter++;
//    if (clapFlag3) {
//        [_audioPlayerClap3 prepareToPlay];
//        [_audioPlayerClap3 play];
//
//    }
}

- (void)playMemos:(NSString *)memo
{
    _audioPlayerClap4 = nil;
    
    _audioPlayerClap4 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.m4a", [[NSBundle mainBundle] resourcePath], memo]] error:nil];
    _audioPlayerClap4.volume = 1.0f;
    _audioPlayerClap4.numberOfLoops = -1;
}


- (void) playRhythmsWithBeats:(AVAudioPlayer*)audioPlayer Beat:(int)beatNumber
{
    if (playFlag == 0) {
        return;
    }
    [audioPlayer prepareToPlay];
    audioPlayer.numberOfLoops = -1;
    audioPlayer.enableRate = true;
    audioPlayer.rate = mCurrentScore/tempo;
    [audioPlayer play];
}

- (void) playSelectedRhythms:(NSString*)playRythm
{
    
    currentRythmName = [countArray objectAtIndex:[_carousel currentItemIndex]];
  
    if(clapFlag3 == 1) {
    //    NSLog(@"the file url: %@ & %@",_audioPlayerClap3,[NSString stringWithFormat:@"%@/Click AccentedNew.wav", [[NSBundle mainBundle] resourcePath]]);
        if (!_audioPlayerClap3) {
            _audioPlayerClap3 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Min_Click_60bpm.m4a", [[NSBundle mainBundle] resourcePath]]] error:nil];
        }
//        float beatFrequency = 60.0 / mCurrentScore;
//        
//        if (!clap3Timer) {
//            clap3Timer = [NSTimer scheduledTimerWithTimeInterval:beatFrequency target:self selector:@selector(clap3timerMethod) userInfo:nil repeats:YES];
//            [[NSRunLoop mainRunLoop] addTimer:clap3Timer forMode:NSRunLoopCommonModes];
//        }
    }
    
    if(clapFlag4 == 1) {
        [self playMemos:_droneType];
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 prepareToPlay];
        [_audioPlayerClap4 playAtTime:myTime];
    }
//    
//    
//    if(clapFlag1 == 0 && clapFlag2 == 0 && clapFlag3 == 0) {
//        //[self playSongCall:1];
//    }
}

 
- (void) stopSelectedRhythms
{
    if(clapFlag1 == 1) {
        _audioPlayerClap1.currentTime = 0;
        [_audioPlayerClap1 stop];
    }
    
    if(clapFlag2 == 1) {
        _audioPlayerClap2.currentTime = 0;
        [_audioPlayerClap2 stop];
    }
    
    if(clapFlag3 == 1) {
        _audioPlayerClap3.currentTime = 0;
        [_audioPlayerClap3 stop];
    }
    
    if(clapFlag4 == 1) {
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 stop];
    }
    
    if(_audioPlayer.isPlaying) {
        _audioPlayer.currentTime = 0;
        [_audioPlayer stop];
    }
}

#pragma mark - UIPickerView Datasource mehods
- (void) setUpBpmPicker {
    
}
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag) {
        return (int)bpmPickerArray.count;
    }
    return (int)[dronePickerArray count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return dronePickerArray[row];
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    if (pickerView.tag) {
        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 50, 27)];
        [lbl setBackgroundColor:[UIColor clearColor]];
        lbl.textColor = [UIColor whiteColor];
        [lbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:25.0]];
        lbl.clipsToBounds = YES;
        lbl.text = bpmPickerArray[row];
        
        [lbl setTextAlignment:NSTextAlignmentCenter];

        return lbl;
    }
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(7, 0, 50, 27)];
    [lbl setBackgroundColor:[UIColor clearColor]];
    lbl.textColor = [UIColor whiteColor];
    [lbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:25.0]];
    
    lbl.text = dronePickerArray[row];
    
    [lbl setTextAlignment:NSTextAlignmentCenter];

    return lbl;
    
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 60;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"row = %@ ",[dronePickerArray objectAtIndex:[pickerView selectedRowInComponent:component]]);
    if (pickerView.tag) {
        mCurrentScore = [[bpmPickerArray objectAtIndex:[pickerView selectedRowInComponent:component]] intValue];
        [self onChangeBPM:nil];
    }
    else{
        _droneType = [dronePickerArray objectAtIndex:[pickerView selectedRowInComponent:component]];
        if (playFlag == 1 && clapFlag4 ==1) {
            [self playMemos:_droneType];
            _audioPlayerClap4.currentTime = 0;
            [_audioPlayerClap4 prepareToPlay];
            [_audioPlayerClap4 play];
        }
    }
    NSLog(@"picker is scrolling and the index number: %ld",(long)[pickerView selectedRowInComponent:component]);
}

#pragma mark- iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [countArray count];
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        CGRect visibleSize = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = visibleSize.size.width;
        if (screenWidth == 414) {
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 250.0f, 80.0f)];  //220
        }else if(screenWidth == 375){
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 220.0f, 80.0f)];  //220
        }else{
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 190.0f, 80.0f)];  //220
        }
        
        // ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeCenter;
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
//        label.font = [label.font fontWithSize:25];
        label.font = [UIFont fontWithName:@"HelveticaNeue" size:21.0];
        label.tag = 1;
       // label.text = [countArray objectAtIndex:index];
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    label.text = [countArray objectAtIndex:index];
    return view;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    // return
    return true;
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.5;
    }
    return value;
}

// to stop sound
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
   
    [_stopBtn setEnabled:YES];
    [_playBtn setBackgroundImage:[UIImage imageNamed:@"PlayIcon"] forState:UIControlStateNormal];
    
    [_playTimerBtn setHidden:YES];
    [_playStopBtn setHidden:YES];
    
    [self stopSelectedRhythms];
    
    [_playTimer invalidate];
    _playTimer = nil;
    
    [beatTimer invalidate];
    beatTimer = nil;
    for (int i = 0; i < 12; i++) {
        UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:i];
        [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
    }
    
    seconds = 0;
    minutes = 0;
    
    //  Never Never
    if ([_carousel currentItemIndex] < 0) {
        _carousel.currentItemIndex = 0;
    }
    if ([countArray count] != 0) {
        [self setDataToUIElements:(int)[_carousel currentItemIndex]];
        caraouselIndex = (int)[_carousel currentItemIndex];
        
        // Default rythm sent to player
        currentRythmName = [countArray objectAtIndex:caraouselIndex];
        float value = [self bpmForSelectedRythm:[countArray objectAtIndex:caraouselIndex]];
        _bpmString = [NSString stringWithFormat:@"%d bpm", (int)value];
        [_bpmPickerView selectRow:((int)value - 60) inComponent:0 animated:NO];
        
        [self enableDropDownLbl:YES];
        
        
        [self resetAllPlayers];
        if (playFlag == 1) {
            playFlag = 0;
//            UIButton *btn;
//            [self onTapPlayBtn:btn];
            [self.playBtn sendActionsForControlEvents: UIControlEventTouchUpInside];
        }
    }
    

}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel{
    NSLog(@"iCarousel is changed");
}

// Set UI elements on Caraousel Item Change
-(void)setDataToUIElements:(int)_index{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    RhythmClass *selectedRhythmClass = [[RhythmClass alloc]init];
    if ([countArray count] != 0) {
        
        currentRythmName = [countArray objectAtIndex:_index];
        
        for (RhythmClass *cls in rhythmArray) {

          //  NSLog(@"cls value for key: %@",[cls valueForKey:@"rhythmName"]);
            if ([[cls valueForKey:@"rhythmName"] isEqualToString:currentRythmName]) {
                appDelegate.latestRhythmClass = cls;
       //         selectedRhythmClass = cls;
                break;
            }
        }
    }
    selectedRhythmClass = appDelegate.latestRhythmClass;
    
    // Set Music
    beatOneMusicFile = selectedRhythmClass.rhythmBeatOne;
    beatTwoMusicFile = selectedRhythmClass.rhythmBeatTwo;
    
    currentRhythmId = [selectedRhythmClass.rhythmId intValue];
    
    int img1 = 1, img2 = 1;
    // Set Image
    if (![selectedRhythmClass.rhythmInstOneImage isEqualToString:@"-1"]) {
        [_instBtn1 setHidden:NO];
        
        //
        NSArray *listItems = [selectedRhythmClass.rhythmInstOneImage componentsSeparatedByString:@"."];
        NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.firstObject];
        
        [_instBtn1 setImage:[UIImage imageNamed:selectedRhythmClass.rhythmInstOneImage] forState:UIControlStateSelected];
        [_instBtn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled.png",lastWordString]] forState:UIControlStateNormal];
        
        img1 = 1;
    }else{
        [_instBtn1 setHidden:YES];
        img1 = 0;
    }
    if (![selectedRhythmClass.rhythmInstTwoImage isEqualToString:@"-1"]) {
        [_instBtn2 setHidden:NO];
        
        //
        NSArray *listItems = [selectedRhythmClass.rhythmInstTwoImage componentsSeparatedByString:@"."];
        NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.firstObject];
        
        [_instBtn2 setImage:[UIImage imageNamed:selectedRhythmClass.rhythmInstTwoImage] forState:UIControlStateSelected];
        [_instBtn2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled.png",lastWordString]] forState:UIControlStateNormal];
        img2 = 1;
    }else{
        [_instBtn2 setHidden:YES];
        img2 = 0;
    }
    
    CGRect visibleSize = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = visibleSize.size.width;
//    CGFloat screenHeight = visibleSize.size.height;
    int xDist = 0;  // for 320
    // If only 2 buttons are there
    if ((img1 == 0) && (img2 == 0)) {
        xDist = ((screenWidth - 120) / 3);
        _audioPlayerClap1 = nil;
        _audioPlayerClap2 = nil;
        _instBtn3.frame = CGRectMake(xDist, _instBtn3.frame.origin.y, 60, 60);
        _bpmPickderBackView.frame = CGRectMake(xDist, _bpmPickderBackView.frame.origin.y, 60, 60);
        _dronBtn.frame = CGRectMake((xDist*2)+60, _dronBtn.frame.origin.y, 60, 60);
        _dronePickerBackView.frame = CGRectMake((xDist*2)+60, _dronePickerBackView.frame.origin.y, 60, 60);
        
        _Instrument3_Layout.constant = xDist;
        _Bpm_Layout.constant =  xDist;
        _Intrument4_Layout.constant = xDist*2 +60;
        _drone_Layout.constant =  xDist*2 +60;
        
        [self.view setNeedsUpdateConstraints];
        
    }else if (img1 == 0){
        xDist = ((screenWidth - 180) / 4);
        _audioPlayerClap1 = nil;
        _instBtn2.frame = CGRectMake(xDist, _instBtn2.frame.origin.y, 60, 60);
        _instBtn3.frame = CGRectMake((xDist*2)+60, _instBtn3.frame.origin.y, 60, 60);
        _bpmPickderBackView.frame = CGRectMake((xDist*2)+60, _bpmPickderBackView.frame.origin.y, 60, 60);
        _dronBtn.frame = CGRectMake((xDist*3)+120, _dronBtn.frame.origin.y, 60, 60);
        _dronePickerBackView.frame = CGRectMake((xDist*3)+120, _dronePickerBackView.frame.origin.y, 60, 60);
        
        _Instrument2_Layout.constant = xDist;
        _Instrument3_Layout.constant = xDist*2 +60;
        _Bpm_Layout.constant =  xDist*2 +60;
        _Intrument4_Layout.constant = xDist*3 +120;
        _drone_Layout.constant =  xDist*3 +120;
    }else if (img2 == 0){
        xDist = ((screenWidth - 180) / 4);
        _audioPlayerClap2 = nil;
        _instBtn1.frame = CGRectMake(xDist, _instBtn1.frame.origin.y, 60, 60);
        _instBtn3.frame = CGRectMake((xDist*2)+60, _instBtn3.frame.origin.y, 60, 60);
        _bpmPickderBackView.frame = CGRectMake((xDist*2)+60, _bpmPickderBackView.frame.origin.y, 60, 60);
        _dronBtn.frame = CGRectMake((xDist*3)+120, _dronBtn.frame.origin.y, 60, 60);
        _dronePickerBackView.frame = CGRectMake((xDist*3)+120, _dronePickerBackView.frame.origin.y, 60, 60);
        
        _Instrument1_Layout.constant = xDist;
        _Instrument3_Layout.constant = xDist*2 +60;
        _Bpm_Layout.constant =  xDist*2 +60;
        _Intrument4_Layout.constant = xDist*3 +120;
        _drone_Layout.constant =  xDist*3 +120;
        
    }else{
        xDist = ((screenWidth - 240) / 5);
        _instBtn1.frame = CGRectMake(xDist, _instBtn1.frame.origin.y, 60, 60);
        _instBtn2.frame = CGRectMake((xDist*2)+60, _instBtn2.frame.origin.y, 60, 60);
        _instBtn3.frame = CGRectMake((xDist*3)+120, _instBtn3.frame.origin.y, 60, 60);
        _bpmPickderBackView.frame = CGRectMake((xDist*3)+120, _bpmPickderBackView.frame.origin.y, 60, 60);
        _dronBtn.frame = CGRectMake((xDist*4)+180, _dronBtn.frame.origin.y, 60, 60);
        _dronePickerBackView.frame = CGRectMake((xDist*4)+180, _dronePickerBackView.frame.origin.y, 60, 60);
        
        _Instrument1_Layout.constant = xDist;
        _Instrument2_Layout.constant = xDist*2 +60;
        _Instrument3_Layout.constant = xDist*3 +120;
        _Bpm_Layout.constant =  xDist*3 +120;
        _Intrument4_Layout.constant = xDist*4 +180;
        _drone_Layout.constant =  xDist*4 +180;
    }
    
    // Set BPM values
    if ([countArray count] != 0) {
        
        float value = [self bpmForSelectedRythm:[countArray objectAtIndex:[_carousel currentItemIndex]]];
        _bpmString = [NSString stringWithFormat:@"%d bpm", (int)value];
        [_bpmTxt setText:_bpmString];
        
        mCurrentScore = [selectedRhythmClass.rhythmStartBPM intValue];
        tempo = value;
        
        [_bpmSlider setValue:value animated:YES];
        _audioPlayerClap1.rate = mCurrentScore/tempo;
        _audioPlayerClap2.rate = mCurrentScore/tempo;
        
    }
    
    // Set Beats Meter
    
    beatCount = [selectedRhythmClass.rhythmBeatsCount intValue];
    if (beatCount != 0) {
        
        // [[UIScreen mainScreen] bounds].size.width
        float XDist = (((screenWidth) - (beatCount*21))/(beatCount+1));
        
        float temp = 0.0;
        
        for (int i = 0; i < beatCount; i++) {
            UIImageView *img = (UIImageView*)[self.beatsView viewWithTag:i];
            temp = lroundf((i *21) + ((i+1)*XDist));
            img.frame = CGRectMake(temp, img.frame.origin.y, 21, 21);
            [img setHidden:NO];
        }
        for (int j = beatCount; j <12; j++) {
            UIImageView *img1 = (UIImageView*)[self.beatsView viewWithTag:j];
            img1.frame = CGRectMake(9000, img1.frame.origin.y, 21, 21);
            [img1 setHidden:YES];
        }
        _beat1Circle_Layout.constant = lroundf((0 *21) + (1*XDist));
        _beat2Circle_Layout.constant = lroundf((1 *21) + (2*XDist));
        _beat3Circle_Layout.constant = lroundf((2 *21) + (3*XDist));
        _beat4Circle_Layout.constant = lroundf((3 *21) + (4*XDist));
        _beat5Circle_Layout.constant = lroundf((4 *21) + (5*XDist));
        _beat6Circle_Layout.constant = lroundf((5 *21) + (6*XDist));
        _beat7Circle_Layout.constant = lroundf((6 *21) + (7*XDist));
        _beat8Circle_Layout.constant = lroundf((7 *21) + (8*XDist));
        _beat9Circle_Layout.constant = lroundf((8 *21) + (9*XDist));
        _beat10Circle_Layout.constant = lroundf((9 *21) + (10*XDist));
        _beat11Circle_Layout.constant = lroundf((10 *21) + (11*XDist));
        _beat12Circle_Layout.constant = lroundf((11 *21) + (12*XDist));
    }
    
    int xMyDist = ((screenWidth - 269) / 2);
    _micGainFirstLeading.constant = xMyDist;
    
    [self.view setNeedsUpdateConstraints];
}

// To learn about notifications, see "Notifications" in Cocoa Fundamentals Guide.
- (void) registerForMediaPlayerNotifications {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_NowPlayingItemChanged:)
                               name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                             object: _musicPlayer];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_PlaybackStateChanged:)
                               name: MPMusicPlayerControllerPlaybackStateDidChangeNotification
                             object: _musicPlayer];
    
    
    [_musicPlayer beginGeneratingPlaybackNotifications];
}

#pragma mark Music notification handlers__________________

// When the now-playing item changes, update the media item artwork and the now-playing label.
- (void) handle_NowPlayingItemChanged: (id) notification {
    
    MPMediaItem *currentItem = [_musicPlayer nowPlayingItem];
    
    // Assume that there is no artwork for the media item.
    UIImage *artworkImage = _noArtworkImage;
    
    // Get the artwork from the current media item, if it has artwork.
    MPMediaItemArtwork *artwork = [currentItem valueForProperty: MPMediaItemPropertyArtwork];
    
    // Obtain a UIImage object from the MPMediaItemArtwork object
    if (artwork) {
        artworkImage = [artwork imageWithSize: CGSizeMake (30, 30)];
    }
    
    // Obtain a UIButton object and set its background to the UIImage object
    UIButton *artworkView = [[UIButton alloc] initWithFrame: CGRectMake (0, 0, 30, 30)];
    [artworkView setBackgroundImage: artworkImage forState: UIControlStateNormal];
    
    // Obtain a UIBarButtonItem object and initialize it with the UIButton object
    UIBarButtonItem *newArtworkItem = [[UIBarButtonItem alloc] initWithCustomView: artworkView];
    [self setArtworkItem: newArtworkItem];
//    [newArtworkItem release];
    
    [_artworkItem setEnabled: NO];
    
    // Display the new media item artwork
  //  [navigationBar.topItem setRightBarButtonItem: artworkItem animated: YES];
    
    // Display the artist and song name for the now-playing media item
    [_nowPlayingLabel setText: [
                               NSString stringWithFormat: @"%@ %@ %@ %@",
                               NSLocalizedString (@"Now Playing:", @"Label for introducing the now-playing song title and artist"),
                               [currentItem valueForProperty: MPMediaItemPropertyTitle],
                               NSLocalizedString (@"by", @"Article between song name and artist name"),
                               [currentItem valueForProperty: MPMediaItemPropertyArtist]]];
    
    if (_musicPlayer.playbackState == MPMusicPlaybackStateStopped) {
        // Provide a suitable prompt to the user now that their chosen music has
        //		finished playing.
        [_nowPlayingLabel setText: [
                                   NSString stringWithFormat: @"%@",
                                   NSLocalizedString (@"Music-ended Instructions", @"Label for prompting user to play music again after it has stopped")]];
        
    }
}

// When the playback state changes, set the play/pause button in the Navigation bar
//		appropriately.
- (void) handle_PlaybackStateChanged: (id) notification {
    
    MPMusicPlaybackState playbackState = [_musicPlayer playbackState];
    
    if (playbackState == MPMusicPlaybackStatePaused) {
        
      //  navigationBar.topItem.leftBarButtonItem = playBarButton;
        
    } else if (playbackState == MPMusicPlaybackStatePlaying) {
        
      //  navigationBar.topItem.leftBarButtonItem = pauseBarButton;
        
    } else if (playbackState == MPMusicPlaybackStateStopped) {
        
      //  navigationBar.topItem.leftBarButtonItem = playBarButton;
        
        // Even though stopped, invoking 'stop' ensures that the music player will play  
        //		its queue from the start.
        [_musicPlayer stop];
        
    }
}

#pragma mark - DropDown setup & delegate methods

- (void)dropDownLblTapAction {
    NSLog(@"lbl tap action");
    [self dropDownLblAction:nil];
}

- (IBAction)dropDownLblAction:(id)sender {
    
    [self.dropDownBgView setHidden:NO];
    [UIView animateWithDuration:0.3 animations:^{
        dropDownObj.frame = self.view.bounds;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)setUpDropDown {
    dropDownObj = [[DropDown alloc]initWithFrame:self.view.bounds];
    dropDownObj.delegate = self;
    dropDownObj.frame = CGRectMake(0, 0, self.view.frame.size.width, 0);
    [self.view addSubview:dropDownObj];
    
}

-(void)closeDropDown {
    [self.dropDownBgView setHidden:YES];
    [UIView animateWithDuration:0.8 animations:^{
        dropDownObj.frame = CGRectMake(0, 0, self.view.frame.size.width, 0);
    } completion:^(BOOL finished) {
        
    }];
}
-(void)dropDownSelectedCell:(NSDictionary *)dct {
    
    [self setDropDownLblWithString:[dct objectForKey:@"selectedString"]];
    
    NSLog(@"the lbl width is: %f",self.dropDownLbl.frame.size.width);
    self.genreIdDict = dct;
    [self closeDropDown];
    [self fetchDBData];
    
    [_carousel reloadData];
    [_carousel scrollToItemAtIndex:carouselFirtValue duration:0.0f];
    
    [self setDataToUIElements:(int)[_carousel currentItemIndex]];
}

-(void)dropDownSameCellSelected{
    [self closeDropDown];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gesture shouldReceiveTouch:(UITouch *)touch {
    
    NSLog(@"this is touch action");
    if (touch.tapCount > 1) { // It will ignore touch with less than two finger
        return YES;
    }
    return YES;
}
- (void)setDropDownLblWithString:(NSString*)string {
    
    _dropDownLbl.text = string;
    [_dropDownLbl sizeToFit];
    _dropDownLbl.center = CGPointMake(self.view.frame.size.width/2-7, 35);
    
    CGRect rect = _arrowImage.frame;
    rect.origin.x = CGRectGetMaxX(_dropDownLbl.frame)+5;
    
    _arrowImage.frame = rect;
}
-(void)enableDropDownLbl:(BOOL)value {
    if (value) {
        _dropDownLbl.alpha = 1;
        _arrowImage.alpha = 1;
        _genreBGView.userInteractionEnabled = YES;
    }
    else {
        _dropDownLbl.alpha = 0.5;
        _arrowImage.alpha = 0.5;
        _genreBGView.userInteractionEnabled = NO;
    }
}

#pragma mark - volume changed action
- (void)volumeChanged:(CGFloat)value {
    NSLog(@"the value: %f",value);
    _musicPlayer.volume = value;
}

@end
