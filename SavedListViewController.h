//
//  SavedListViewController.h
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <AVFoundation/AVFoundation.h>
#include <MediaPlayer/MPVolumeView.h>
#include <MediaPlayer/MPMusicPlayerController.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "DBManager.h"
#import <iAd/iAd.h>
//#import "GADBannerView.h"
#import "SavedListDetailViewController.h"

@import GoogleMobileAds;

@class MainNavigationViewController;

@interface SavedListViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,AVAudioPlayerDelegate,ADBannerViewDelegate,GADBannerViewDelegate,ExpandedCellDelegate>
{
     // array of arrays
    NSMutableArray *songList;
   
    NSString *currentRythmName,*songDuration,*dateOfRecording,*songDetail,*durationStringUnFormatted;
   
    BOOL bannerStatus;
    DBManager *sqlManager;
    
}

@property (strong, nonatomic) IBOutlet UITableView *songTableView;

@property (strong, nonatomic) NSString *selctRow;

@property (strong, nonatomic) MainNavigationViewController *myNavigationController;

@property (weak, nonatomic) IBOutlet ADBannerView *bannerView;
@property (nonatomic, strong) GADBannerView *admobBannerView;

//- (IBAction)shareBtnAction:(id)sender;
//- (IBAction)OnChangeVolumeSlider:(id)sender;

@end
