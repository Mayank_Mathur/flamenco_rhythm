//
//  AppDelegate.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "RecordViewController.h"
#import "SavedListViewController.h"
#import "RhythmClass.h"

const float xPercCircles[15] = {7.96, 9.84, 13.43, 18.59, 25.15, 32.81, 41.25, 50, 58.75, 67.18, 74.84, 81.40, 86.56, 90.15, 92.03};
const float yPercCircles[15] = {73.23, 68.39, 63.90, 59.85, 56.51, 54.04, 52.55, 52.02, 52.55, 54.04, 56.51, 59.85, 63.90, 68.39, 73.23};

@interface AppDelegate (){
    
}

@end

@implementation AppDelegate
@synthesize myNavigationController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    application.idleTimerDisabled = YES;
    _latestRhythmClass = [[RhythmClass alloc]init];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    myNavigationController = [mainStoryboard instantiateViewControllerWithIdentifier:@"navigationVC"];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window.rootViewController = myNavigationController;
    
//    CGRect visibleSize = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = visibleSize.size.width;
//    CGFloat screenHeight = visibleSize.size.height;
//    NSLog(@"Frame width = %f",screenWidth);
//    
//    for (int i = 1; i <14; i++) {
//        UIImageView *img = [[UIImageView alloc]init];
//        img.frame = CGRectMake((screenWidth * xPercCircles[i]/100)-10, ((screenHeight * yPercCircles[i]/100)-11), 21, 21);
//        NSLog(@"img Frame = %@",NSStringFromCGRect(img.frame));
//        // [img setImage:[UIImage imageNamed:@"Claps4_Gray.png"]];
//    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
