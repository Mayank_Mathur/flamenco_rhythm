//
//  DBManager.h
//  FlamencoRhythm
//
//  Created by Sajiv Nair on 16/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#ifndef FlamencoRhythm_DBManager_h
#define FlamencoRhythm_DBManager_h

#import <sqlite3.h>

#endif
@class GenreClass;

@interface DBManager : NSObject

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

-(DBManager*)initWithDatabaseFilename:(NSString *)dbFilename;
-(void)copyDatabaseIntoDocumentsDirectory;

- (BOOL)insertDataToRecordingInDictionary:(NSDictionary*)dict;
- (BOOL)saveData1;
-(int)getRowCount:(NSString*)tableName;
-(BOOL)createRecordingTable;
-(BOOL)createGenreTable;
-(BOOL)createRhythmTable;
-(NSMutableArray *)getRhythmsFromGenre:(NSString*)genre;
-(NSMutableArray*)getRhythmRecords:(NSNumber*)genreId;
-(NSDictionary*)getAudioFileRecords;

// Created by AG.
- (BOOL)updateSingleRecordingDataWithRecordingId :(int)recId trackSequence :(int)sequence track :(NSString *)trackPath maxTrackDuration :(NSString *)duration trackDuration :(NSString *)tDuration;
- (BOOL)updateDeleteRecordOfRecordID :(NSNumber *)recordID;
- (BOOL)updateRecordingNameOfRecordID :(NSNumber *)recordID updatedName :(NSString *)name;


-(NSArray*) getDroneName;

// Rasool's Method
-(void)isDBOpened;
-(NSMutableArray *)getAllGenreDetails;
- (NSMutableArray *)getAllRecordingData;
// by AG
-(NSMutableArray*)fetchRhythmRecordsByID:(NSNumber*)rythmId;
@end
