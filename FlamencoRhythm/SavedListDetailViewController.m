//
//  SavedListDetailViewController.m
//  FlamencoRhythm
//
//  Created by Ashish Gore on 21/05/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "SavedListDetailViewController.h"
#import "RAAlertController.h"

@interface SavedListDetailViewController ()
@property (strong, nonatomic) RAAlertController *alertController;
@end

@implementation SavedListDetailViewController

#pragma mark - View management

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUIElements];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    int img1 = 1, img2 = 1;
    // Set Image
    if (![rhythmRecord.rhythmInstOneImage isEqualToString:@"-1"]) {
        [_instrument1 setHidden:NO];
        [_volImageInstru1 setHidden:NO];
        [_instrument1 setImage:[UIImage imageNamed:rhythmRecord.rhythmInstOneImage] forState:UIControlStateNormal];
        [_instrument1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",rhythmRecord.rhythmInstOneImage]] forState:UIControlStateSelected];
        
        img1 = 1;
    }else{
        [_instrument1 setHidden:YES];
        [_volImageInstru1 setHidden:YES];
        img1 = 0;
    }
    if (![rhythmRecord.rhythmInstTwoImage isEqualToString:@"-1"]) {
        [_instrument2 setHidden:NO];
        [_volImageInstru2 setHidden:NO];
        [_instrument2 setImage:[UIImage imageNamed:rhythmRecord.rhythmInstTwoImage] forState:UIControlStateNormal];
        [_instrument2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",rhythmRecord.rhythmInstTwoImage]] forState:UIControlStateSelected];
        
        img2 = 1;
    }else{
        [_instrument2 setHidden:YES];
        [_volImageInstru2 setHidden:YES];
        img2 = 0;
    }
    
    CGRect visibleSize = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = visibleSize.size.width;
    //    CGFloat screenHeight = visibleSize.size.height;
    int xDist = 0;  // for 320
    // If only 2 buttons are there
    if ((img1 == 0) && (img2 == 0)) {
        xDist = ((screenWidth - 120) / 3);
        _audioPlayerClap1 = nil;
        _audioPlayerClap2 = nil;
        _instrument3.frame = CGRectMake(xDist, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake(xDist, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*2)+60, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;
        //_dronePickerBackView.frame = CGRectMake((xDist*2)+60, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument3_Layout.constant = xDist;
        //            _Bpm_Layout.constant =  xDist;
        //            _Intrument4_Layout.constant = xDist*2 +60;
        //            _drone_Layout.constant =  xDist*2 +60;
        
        //[self.view setNeedsUpdateConstraints];
        
    }else if (img1 == 0){
        xDist = ((screenWidth - 180) / 4);
        _audioPlayerClap1 = nil;
        _instrument2.frame = CGRectMake(xDist, _instrument2.frame.origin.y, 60, 60);
        _instrument3.frame = CGRectMake((xDist*2)+60, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake((xDist*2)+60, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*3)+120, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru2.center = _instrument2.center;
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;
        //_dronePickerBackView.frame = CGRectMake((xDist*3)+120, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument2_Layout.constant = xDist;
        //            _Instrument3_Layout.constant = xDist*2 +60;
        //            _Bpm_Layout.constant =  xDist*2 +60;
        //            _Intrument4_Layout.constant = xDist*3 +120;
        //            _drone_Layout.constant =  xDist*3 +120;
    }else if (img2 == 0){
        xDist = ((screenWidth - 180) / 4);
        _audioPlayerClap2 = nil;
        _instrument1.frame = CGRectMake(xDist, _instrument1.frame.origin.y, 60, 60);
        _instrument3.frame = CGRectMake((xDist*2)+60, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake((xDist*2)+60, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*3)+120, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru1.center = _instrument1.center;
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;
        //_dronePickerBackView.frame = CGRectMake((xDist*3)+120, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument1_Layout.constant = xDist;
        //            _Instrument3_Layout.constant = xDist*2 +60;
        //            _Bpm_Layout.constant =  xDist*2 +60;
        //            _Intrument4_Layout.constant = xDist*3 +120;
        //            _drone_Layout.constant =  xDist*3 +120;
        
    }else{
        xDist = ((screenWidth - 240) / 5);
        _instrument1.frame = CGRectMake(xDist, _instrument1.frame.origin.y, 60, 60);
        _instrument2.frame = CGRectMake((xDist*2)+60, _instrument2.frame.origin.y, 60, 60);
        _instrument3.frame = CGRectMake((xDist*3)+120, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake((xDist*3)+120, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*4)+180, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru1.center = _instrument1.center;
        _volImageInstru2.center = _instrument2.center;
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;        //_dronePickerBackView.frame = CGRectMake((xDist*4)+180, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument1_Layout.constant = xDist;
        //            _Instrument2_Layout.constant = xDist*2 +60;
        //            _Instrument3_Layout.constant = xDist*3 +120;
        //            _Bpm_Layout.constant =  xDist*3 +120;
        //            _Intrument4_Layout.constant = xDist*4 +180;
        //            _drone_Layout.constant =  xDist*4 +180;
    }
    
    if(clapFlag1 == 1)
    {
        _instrument1.selected = YES;
    }
    else
        _instrument1.selected = NO;
    
    if(clapFlag2 == 1)
    {
        _instrument2.selected = YES;
    }
    else
        _instrument2.selected = NO;
    
    if(clapFlag3 == 1)
    {
        _instrument3.selected = YES;
    }
    else
        _instrument3.selected = NO;
    
    if(clapFlag4 == 1)
    {
        _instrument4.selected = YES;
    }
    else
        _instrument4.selected = NO;
    
    if(recFlag1 == 1)
    {
        _firstVolumeKnob.selected = YES;
    }
    else
        _firstVolumeKnob.selected = NO;
    
    if(recFlag2 == 1)
    {
        _secondVolumeKnob.selected = YES;
    }
    else
        _secondVolumeKnob.selected = NO;
    
    if(recFlag3 == 1)
    {
        _thirdVolumeKnob.selected = YES;
    }
    else
        _thirdVolumeKnob.selected = NO;
    
    if(recFlag4 == 1)
    {
        _fourthVolumeKnob.selected = YES;
    }
    else
        _fourthVolumeKnob.selected = NO;
    
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sqlManager = [[DBManager alloc] init];
    
    upRecognizerInst1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerInst1 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerInst1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerInst1 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerInst2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerInst2 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerInst2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerInst2 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerInst3 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerInst3 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerInst3 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerInst3 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerInst4 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerInst4 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerInst4 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerInst4 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerRec1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerRec1 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerRec1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerRec1 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerRec2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerRec2 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerRec2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerRec2 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerRec3 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerRec3 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerRec3 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerRec3 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    upRecognizerRec4 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [upRecognizerRec4 setDirection: UISwipeGestureRecognizerDirectionUp];
    
    downRecognizerRec4 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedScreen:)];
    [downRecognizerRec4 setDirection: UISwipeGestureRecognizerDirectionDown];
    
    recFlag1 = 1;
    
    audioPlayerArray = [[NSMutableArray alloc] init];
    recordingDurationArray = [[NSMutableArray alloc] init];
    
    _musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    
    [_musicPlayer beginGeneratingPlaybackNotifications];
    
    longPressForKnob1 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(LongPress:)];
    longPressForKnob1.minimumPressDuration = .5; //seconds
    longPressForKnob1.delegate = self;
    
    longPressForKnob2 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(LongPress:)];
    longPressForKnob2.minimumPressDuration = .5; //seconds
    longPressForKnob2.delegate = self;
    
    longPressForKnob3 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(LongPress:)];
    longPressForKnob3.minimumPressDuration = .5; //seconds
    longPressForKnob3.delegate = self;
    
    longPressForKnob4 = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(LongPress:)];
    longPressForKnob4.minimumPressDuration = .5; //seconds
    longPressForKnob4.delegate = self;
    
    [_instrument1 addGestureRecognizer:upRecognizerInst1];
    [_instrument1 addGestureRecognizer:downRecognizerInst1];
    
    [_instrument2 addGestureRecognizer:upRecognizerInst2];
    [_instrument2 addGestureRecognizer:downRecognizerInst2];
    
    [_instrument3 addGestureRecognizer:upRecognizerInst3];
    [_instrument3 addGestureRecognizer:downRecognizerInst3];
    
    [_instrument4 addGestureRecognizer:upRecognizerInst4];
    [_instrument4 addGestureRecognizer:downRecognizerInst4];
    
    [_firstVolumeKnob addGestureRecognizer:upRecognizerRec1];
    [_firstVolumeKnob addGestureRecognizer:downRecognizerRec1];
    
    [_secondVolumeKnob addGestureRecognizer:upRecognizerRec2];
    [_secondVolumeKnob addGestureRecognizer:downRecognizerRec2];
    
    [_thirdVolumeKnob addGestureRecognizer:upRecognizerRec3];
    [_thirdVolumeKnob addGestureRecognizer:downRecognizerRec3];
    
    [_fourthVolumeKnob addGestureRecognizer:upRecognizerRec4];
    [_fourthVolumeKnob addGestureRecognizer:downRecognizerRec4];
    
    [_firstVolumeKnob addGestureRecognizer:longPressForKnob1];
    [_secondVolumeKnob addGestureRecognizer:longPressForKnob2];
    [_thirdVolumeKnob addGestureRecognizer:longPressForKnob3];
    [_fourthVolumeKnob addGestureRecognizer:longPressForKnob4];
    
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    _recorder.meteringEnabled = YES;
    [_recorder prepareToRecord];
    
    _session = [AVAudioSession sharedInstance];
    [_session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    for (_input in [_session availableInputs]) {
        // set as an input the build-in microphone
        
        if ([_input.portType isEqualToString:AVAudioSessionPortBuiltInMic]) {
            _myPort = _input;
            break;
        }
    }
    
    [_session setPreferredInput:_myPort error:nil];
    // [_session setPreferredInput:_myPort error:&audioSessionError];
    //  [self setupApplicationAudio];
    [self registerForMediaPlayerNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIGesture
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer  {
    return YES;
}

#pragma mark - notifications

// To learn about notifications, see "Notifications" in Cocoa Fundamentals Guide.
- (void) registerForMediaPlayerNotifications {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_NowPlayingItemChanged:)
                               name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                             object: _musicPlayer];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_PlaybackStateChanged:)
                               name: MPMusicPlayerControllerPlaybackStateDidChangeNotification
                             object: _musicPlayer];
    
    /*
     // This sample doesn't use libray change notifications; this code is here to show how
     //		it's done if you need it.
     [notificationCenter addObserver: self
     selector: @selector (handle_iPodLibraryChanged:)
     name: MPMediaLibraryDidChangeNotification
     object: musicPlayer];
     
     [[MPMediaLibrary defaultMediaLibrary] beginGeneratingLibraryChangeNotifications];
     */
    
    [_musicPlayer beginGeneratingPlaybackNotifications];
}

#pragma mark -  Music notification handlers

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    NSLog(@"Finished player");
    
    if (![_recorder isRecording]) {
        
        [self resetPlayButtonWithCell];
    }
}

// When the now-playing item changes, update the media item artwork and the now-playing
- (void) handle_NowPlayingItemChanged: (id) notification {
    
    MPMediaItem *currentItem = [_musicPlayer nowPlayingItem];
    
    // Assume that there is no artwork for the media item.
    UIImage *artworkImage = _noArtworkImage;
    
    // Get the artwork from the current media item, if it has artwork.
    MPMediaItemArtwork *artwork = [currentItem valueForProperty: MPMediaItemPropertyArtwork];
    
    // Obtain a UIImage object from the MPMediaItemArtwork object
    if (artwork) {
        artworkImage = [artwork imageWithSize: CGSizeMake (30, 30)];
    }
    
    // Obtain a UIButton object and set its background to the UIImage object
    UIButton *artworkView = [[UIButton alloc] initWithFrame: CGRectMake (0, 0, 30, 30)];
    [artworkView setBackgroundImage: artworkImage forState: UIControlStateNormal];
    
    // Obtain a UIBarButtonItem object and initialize it with the UIButton object
    UIBarButtonItem *newArtworkItem = [[UIBarButtonItem alloc] initWithCustomView: artworkView];
    [self setArtworkItem: newArtworkItem];
    //    [newArtworkItem release];
    
    [_artworkItem setEnabled: NO];
    
    // Display the new media item artwork
    //  [navigationBar.topItem setRightBarButtonItem: artworkItem animated: YES];
    
    // Display the artist and song name for the now-playing media item
    [_nowPlayingLabel setText: [
                                NSString stringWithFormat: @"%@ %@ %@ %@",
                                NSLocalizedString (@"Now Playing:", @"Label for introducing the now-playing song title and artist"),
                                [currentItem valueForProperty: MPMediaItemPropertyTitle],
                                NSLocalizedString (@"by", @"Article between song name and artist name"),
                                [currentItem valueForProperty: MPMediaItemPropertyArtist]]];
    
    if (_musicPlayer.playbackState == MPMusicPlaybackStateStopped) {
        // Provide a suitable prompt to the user now that their chosen music has
        //		finished playing.
        [_nowPlayingLabel setText: [
                                    NSString stringWithFormat: @"%@",
                                    NSLocalizedString (@"Music-ended Instructions", @"Label for prompting user to play music again after it has stopped")]];
        
    }
}

// When the playback state changes, set the play/pause button in the Navigation bar
//		appropriately.
- (void) handle_PlaybackStateChanged: (id) notification {
    
    MPMusicPlaybackState playbackState = [_musicPlayer playbackState];
    
    if (playbackState == MPMusicPlaybackStatePaused) {
        
        //  navigationBar.topItem.leftBarButtonItem = playBarButton;
        
    } else if (playbackState == MPMusicPlaybackStatePlaying) {
        
        //  navigationBar.topItem.leftBarButtonItem = pauseBarButton;
        
    } else if (playbackState == MPMusicPlaybackStateStopped) {
        
        //  navigationBar.topItem.leftBarButtonItem = playBarButton;
        
        // Even though stopped, invoking 'stop' ensures that the music player will play
        //		its queue from the start.
        [_musicPlayer stop];
        
    }
}



#pragma mark - UIButtonActions------

- (IBAction)menuBtnClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Share" destructiveButtonTitle:@"Delete" otherButtonTitles:nil];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (IBAction)shareBtnAction:(id)sender {
    NSLog(@"this is share action");
}

- (void)itemWasTouchedUpAndDidHold:(id)sender
{
    UIControl *control = sender ;
    int tag = (int)control.tag;
    NSLog(@"tag = %d",tag);
    
    if(tag == 11)
    {
        control.hidden = YES;
        control.center = secondKnobCentre;
        secondKnob.center = thirdKnobCentre;
        thirdKnob.center = forthKnobCentre;
        forthKnob.center = forthKnobCentre;
        
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             // set the new frame
                             control.hidden = NO;
                             control.center = firstKnobCentre;
                             secondKnob.center = secondKnobCentre;
                             thirdKnob.center = thirdKnobCentre;
                             forthKnob.hidden = YES;
                             [_deleteImageT1 setHidden:YES];
                             [_deleteBGView setHidden:YES];
                             [self.view sendSubviewToBack:_firstVolumeKnob];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             //sleep(1);
                             forthKnob.hidden = NO;
                             forthKnob.center = forthKnobCentre;
                             
                             if ([_recTrackTwo isEqualToString:@"-1"]) {
                                 
                                 recFlag1 = 0;
                                 recFlag2 = 0;
                                 recFlag3 = 0;
                                 recFlag4 = 0;
                                 
                                 t1Duration = @"-1";
                                 durationStringUnFormatted = t1Duration;
                                 _TotalTimeLbl.text = @"00:00";
                                 _maxRecDurationLbl.text = @"00:00";
                                 
                                 [_firstVolumeKnob setSelected:NO];
                                 [_secondVolumeKnob setSelected:NO];
                                 [_thirdVolumeKnob setSelected:NO];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_redoutline.png"] forState:UIControlStateNormal];
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_greyoutline.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
                             }
                             else if ([_recTrackThree isEqualToString:@"-1"])
                             {
                                 recFlag1 = 1;
                                 recFlag2 = 0;
                                 recFlag3 = 0;
                                 recFlag4 = 0;
                                 t1Duration = t2Duration;
                                 t3Duration = @"-1";
                                 durationStringUnFormatted = t1Duration;
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 [_firstVolumeKnob setSelected:YES];
                                 [_secondVolumeKnob setSelected:NO];
                                 [_thirdVolumeKnob setSelected:NO];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_darkgrey.png"] forState:UIControlStateNormal];
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_redoutline.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
                             }
                             else if ([_recTrackFour isEqualToString:@"-1"])
                             {
                                 recFlag1 = 1;
                                 recFlag2 = 1;
                                 recFlag3 = 0;
                                 recFlag4 = 0;
                                 
                                 t1Duration = t2Duration;
                                 t2Duration = t3Duration;
                                 t3Duration = @"-1";
                                 t4Duration = @"-1";
                                 
                                 if ([t2Duration intValue] > [t1Duration intValue]) {
                                     durationStringUnFormatted = t2Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 [_firstVolumeKnob setSelected:YES];
                                 [_secondVolumeKnob setSelected:YES];
                                 [_thirdVolumeKnob setSelected:NO];
                                 [_fourthVolumeKnob setSelected:NO];
                                 [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_darkgrey.png"] forState:UIControlStateNormal];
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_darkgrey.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_redoutline.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
                             }
                             else
                             {
                                 recFlag1 = 1;
                                 recFlag2 = 1;
                                 recFlag3 = 1;
                                 recFlag4 = 0;
                                 
                                 t1Duration = t2Duration;
                                 t2Duration = t3Duration;
                                 t3Duration = t4Duration;
                                 t4Duration = @"-1";
                                 
                                 
                                 if ([t3Duration intValue] > [t2Duration intValue]) {
                                     if ([t3Duration intValue] > [t1Duration intValue])
                                     {
                                         durationStringUnFormatted = t3Duration;
                                     }
                                     else
                                         durationStringUnFormatted = t1Duration;
                                 }
                                 else if ([t2Duration intValue] > [t1Duration intValue])
                                 {
                                     durationStringUnFormatted = t2Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                                 
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 
                                 [_firstVolumeKnob setSelected:YES];
                                 [_secondVolumeKnob setSelected:YES];
                                 [_thirdVolumeKnob setSelected:YES];
                                 [_fourthVolumeKnob setSelected:NO];
                                 [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_darkgrey.png"] forState:UIControlStateNormal];
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_darkgrey.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_darkgrey.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
                             }
                             
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:1 track:_recTrackTwo maxTrackDuration:durationStringUnFormatted trackDuration:t1Duration];
                             _recTrackOne = _recTrackTwo;
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:2 track:_recTrackThree maxTrackDuration:durationStringUnFormatted trackDuration:t2Duration];
                             _recTrackTwo = _recTrackThree;
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:3 track:_recTrackFour maxTrackDuration:durationStringUnFormatted trackDuration:t3Duration];
                             _recTrackThree = _recTrackFour;
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:4 track:@"-1" maxTrackDuration:durationStringUnFormatted trackDuration:t4Duration];
                             _recTrackFour = @"-1";
                             
                             
                         }
         ];
    }
    
    if(tag == 22)
    {
        control.hidden = YES;
        control.center = thirdKnobCentre;
        //secondKnob.center = thirdKnobCentre;
        thirdKnob.center = forthKnobCentre;
        forthKnob.center = forthKnobCentre;
        
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             // set the new frame
                             control.hidden = NO;
                             control.center = secondKnobCentre;
                             //secondKnob.center = secondKnobCentre;
                             thirdKnob.center = thirdKnobCentre;
                             forthKnob.hidden = YES;
                             [_deleteImageT2 setHidden:YES];
                             [_deleteBGView setHidden:YES];
                             [self.view sendSubviewToBack:_secondVolumeKnob];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             //sleep(1);
                             forthKnob.hidden = NO;
                             forthKnob.center = forthKnobCentre;
                             
                             
                             
                             //[_secondVolumeKnob setHighlighted:NO];
                             //[_secondVolumeKnob setSelected:YES];
                             if ([_recTrackThree isEqualToString:@"-1"]) {
                                 recFlag2 = 0;
                                 t2Duration = @"-1";
                                 durationStringUnFormatted = t1Duration;
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 [_secondVolumeKnob setSelected:NO];
                                 [_thirdVolumeKnob setSelected:NO];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_redoutline.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
                                 
                             }
                             else if ([_recTrackFour isEqualToString:@"-1"])
                             {
                                 
                                 recFlag2 = 1;
                                 recFlag3 = 0;
                                 recFlag4 = 0;
                                 t2Duration  = t3Duration;
                                 t3Duration = @"-1";
                                 t4Duration = @"-1";
                                 
                                 if ([t2Duration intValue] > [t1Duration intValue]) {
                                     durationStringUnFormatted = t2Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                                 
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 [_secondVolumeKnob setSelected:YES];
                                 [_thirdVolumeKnob setSelected:NO];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_darkgrey.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_redoutline.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
                             }
                             else
                             {
                                 
                                 recFlag2 = 1;
                                 recFlag3 = 1;
                                 recFlag4 = 0;
                                 
                                 t2Duration = t3Duration;
                                 t3Duration = t4Duration;
                                 t3Duration = @"-1";
                                 
                                 if ([t3Duration intValue] > [t2Duration intValue]) {
                                     if ([t3Duration intValue] > [t1Duration intValue])
                                     {
                                         durationStringUnFormatted = t3Duration;
                                     }
                                     else
                                         durationStringUnFormatted = t1Duration;
                                 }
                                 else if ([t2Duration intValue] > [t1Duration intValue])
                                 {
                                     durationStringUnFormatted = t2Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                                 
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 [_secondVolumeKnob setSelected:YES];
                                 [_thirdVolumeKnob setSelected:YES];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_darkgrey.png"] forState:UIControlStateNormal];
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_darkgrey.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
                             }
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:2 track:_recTrackThree maxTrackDuration:durationStringUnFormatted trackDuration:t2Duration];
                             _recTrackTwo = _recTrackThree;
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:3 track:_recTrackFour maxTrackDuration:durationStringUnFormatted trackDuration:t3Duration];
                             _recTrackThree = _recTrackFour;
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:4 track:@"-1" maxTrackDuration:durationStringUnFormatted trackDuration:t4Duration];
                             _recTrackFour = @"-1";
                         }
         ];
    }
    
    if(tag == 33)
    {
        control.hidden = YES;
        control.center = forthKnobCentre;
        //secondKnob.center = thirdKnobCentre;
        //thirdKnob.center = forthKnobCentre;
        forthKnob.center = forthKnobCentre;
        
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             // set the new frame
                             control.hidden = NO;
                             control.center = thirdKnobCentre;
                             //secondKnob.center = secondKnobCentre;
                             //thirdKnob.center = thirdKnobCentre;
                             forthKnob.hidden = YES;
                             [_deleteImageT3 setHidden:YES];
                             [_deleteBGView setHidden:YES];
                             [self.view sendSubviewToBack:_thirdVolumeKnob];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             //sleep(1);
                             forthKnob.hidden = NO;
                             forthKnob.center = forthKnobCentre;
                             
                             
                             if ([_recTrackFour isEqualToString:@"-1"]) {
                                 recFlag3 = 0;
                                 t3Duration = @"-1";
                                 if ([t2Duration intValue] > [t1Duration intValue]) {
                                     durationStringUnFormatted = t2Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                                 
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                
                                 
                                 [_thirdVolumeKnob setSelected:NO];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_redoutline.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
                                 
                             }
                             else
                             {
                                 recFlag3 = 1;
                                 recFlag4 = 0;
                                 
                                 t3Duration = t4Duration;
                                 t4Duration = @"-1";
                                 
                                 if ([t3Duration intValue] > [t2Duration intValue]) {
                                     if ([t3Duration intValue] > [t1Duration intValue])
                                     {
                                         durationStringUnFormatted = t3Duration;
                                     }
                                     else
                                         durationStringUnFormatted = t1Duration;
                                 }
                                 else if ([t2Duration intValue] > [t1Duration intValue])
                                 {
                                     durationStringUnFormatted = t2Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                                 
                                 [self updateUIDataWithDuration:durationStringUnFormatted];
                                 
                                 [_thirdVolumeKnob setSelected:YES];
                                 [_fourthVolumeKnob setSelected:NO];
                                 
                                 [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_darkgrey.png"] forState:UIControlStateNormal];
                                 [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
                                 
                             }
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:3 track:_recTrackFour maxTrackDuration:durationStringUnFormatted trackDuration:t3Duration];
                             _recTrackThree = _recTrackFour;
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:4 track:@"-1" maxTrackDuration:durationStringUnFormatted trackDuration:t4Duration];
                             _recTrackFour = @"-1";
                             
                         }
         ];
    }
    if(tag == 44)
    {
        control.hidden = YES;
        //control.center = forthKnobCentre;
        //secondKnob.center = thirdKnobCentre;
        //thirdKnob.center = forthKnobCentre;
        //forthKnob.center = forthKnobCentre;
        //control.center = CGPointMake(forthKnobCentre.x +100, forthKnobCentre.y);
        
        
        [UIView animateWithDuration:0.5
                              delay:0
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             // set the new frame
                             control.hidden = NO;
                             control.center = forthKnobCentre;
                             [_deleteImageT4 setHidden:YES];
                             [_deleteBGView setHidden:YES];
                             [self.view sendSubviewToBack:_fourthVolumeKnob];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                             recFlag4 = 0;
                             t4Duration = @"-1";
                             if ([t3Duration intValue] > [t2Duration intValue]) {
                                 if ([t3Duration intValue] > [t1Duration intValue])
                                 {
                                     durationStringUnFormatted = t3Duration;
                                 }
                                 else
                                     durationStringUnFormatted = t1Duration;
                             }
                             else if ([t2Duration intValue] > [t1Duration intValue])
                             {
                                 durationStringUnFormatted = t2Duration;
                             }
                             else
                                 durationStringUnFormatted = t1Duration;
                             
                             [self updateUIDataWithDuration:durationStringUnFormatted];
                             
                             [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue]  trackSequence:4 track:@"-1" maxTrackDuration:durationStringUnFormatted trackDuration:t4Duration];
                             _recTrackFour = @"-1";
                             
                             [_fourthVolumeKnob setSelected:NO];
                             [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
                             
                         }
         ];
    }
    
    
}

- (void)itemWasTouchedUp:(id)sender
{
    UIButton *button = sender;
    
    if (button.tag == 11 && ![_recTrackOne isEqualToString:@"-1"]) {
        if (recFlag1 == 0 )
        {
            [button setSelected:YES];
//            [button setBackgroundImage:[UIImage imageNamed:@"one_blue.png"] forState:UIControlStateNormal];
            recFlag1 = 1;
            if(playFlag == 1)
            {
                //[self playSelectedRecording];
                _recAudioPlayer1.volume = _musicPlayer.volume;
            }
        } else {
            [button setSelected:NO];
//            [button setBackgroundImage:[UIImage imageNamed:@"one_darkgrey.png"] forState:UIControlStateNormal];
            // _recAudioPlayer1.currentTime = 0;
            //[_recAudioPlayer1 stop];
            recFlag1 = 0;
            _recAudioPlayer1.volume = 0;
        }
    }
    if (button.tag == 22 && ![_recTrackTwo isEqualToString:@"-1"]) {
        if (recFlag2 == 0) {
            [button setSelected:YES];
//            [button setBackgroundImage:[UIImage imageNamed:@"two_blue.png"] forState:UIControlStateNormal];
            recFlag2 = 1;
            if(playFlag == 1)
            {
                // [self playSelectedRecording];
                _recAudioPlayer2.volume = _musicPlayer.volume;
            }
        } else {
            //[button setSelected:NO];
//            [button setBackgroundImage:[UIImage imageNamed:@"two_darkgrey.png"] forState:UIControlStateNormal];
            [button setSelected:NO];
            _recAudioPlayer2.volume = 0;
            recFlag2 = 0;
        }
    }
    if (button.tag == 33 && ![_recTrackThree isEqualToString:@"-1"]) {
        if (recFlag3 == 0) {
            [button setSelected:YES];
//            [button setBackgroundImage:[UIImage imageNamed:@"three_blue.png"] forState:UIControlStateNormal];
            recFlag3 = 1;
            if(playFlag == 1)
            {
                //[self playSelectedRecording];
                _recAudioPlayer3.volume = _musicPlayer.volume;
            }
        } else {
            [button setSelected:NO];
//            [button setBackgroundImage:[UIImage imageNamed:@"three_darkgrey.png"] forState:UIControlStateNormal];
            _recAudioPlayer3.volume = 0;
            recFlag3 = 0;
        }
    }
    if (button.tag == 44 && ![_recTrackFour isEqualToString:@"-1"]) {
        if (recFlag4 == 0) {
            [button setSelected:YES];
//            [button setBackgroundImage:[UIImage imageNamed:@"four_blue.png"] forState:UIControlStateNormal];
            recFlag4 = 1;
            if(playFlag == 1)
            {
                // [self playSelectedRecording];
                _recAudioPlayer4.volume = _musicPlayer.volume;
            }
        } else {
            [button setSelected:NO];
//            [button setBackgroundImage:[UIImage imageNamed:@"four_darkgrey.png"] forState:UIControlStateNormal];
            _recAudioPlayer4.volume = 0;
            recFlag4 = 0;
        }
    }
}

-(IBAction)itemTouchUpInside:(id)sender {
    if (didHold) {
        didHold = NO;
        [self itemWasTouchedUpAndDidHold:sender];
    } else {
        didHold = NO;
        [self itemWasTouchedUp:sender];
    }
}

-(IBAction)closeButtonClicked:(UIButton*)sender
{
    [self.delegate expandedCellWillCollapse];
<<<<<<< HEAD
=======
    
>>>>>>> 5be2d5c177e7d99247aeecae86957aedb6cf421f
}


-(IBAction)playButtonClicked:(UIButton*)sender
{
    
    if (playFlag == 0) {
        [_playRecBtn setBackgroundImage:[UIImage imageNamed:@"stopicon.png"] forState:UIControlStateNormal];
        [_recordingBtn setEnabled:NO];
        [self playSelectedRecording];
        
        playFlag =1;
        
        _recSlider.maximumValue = [durationStringUnFormatted floatValue];
        
        _recordTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f
                                                        target: self
                                                      selector:@selector(onPlayOrRecordTimer:)
                                                      userInfo: nil repeats:YES];
        [_recordTimer fire];
        
        _updateSliderTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self selector:@selector(updateSlider:) userInfo:nil repeats:YES];
        [_updateSliderTimer fire];
        
    }
    else if (playFlag == 1)
    {
        [self resetPlayButtonWithCell];
        
    }
    
    
}

- (IBAction)recordingButtonClicked:(id)sender
{
    
    [_recordingBGView setHidden:NO];
    micArray = [[NSArray alloc]initWithObjects:_mic1,_mic2,_mic3,_mic4,_mic5,_mic6,_mic7,_mic8,_mic9,_mic10, nil];
    
    if (stopFlag == 0) {
        
        [_playRecBtn setSelected:YES];
        [_recordingBtn setSelected:YES];
        
        [_recSlider setThumbImage:[UIImage imageNamed:@"volumesqaure_pointer_red.png"] forState:UIControlStateNormal];
        [_recSlider setMaximumTrackImage:[UIImage imageNamed:@"volumeslider_sqaure_gred.png"] forState:UIControlStateNormal];
        [_recSlider setMinimumTrackImage:[UIImage imageNamed:@"volumeslider_sqaure_red.png"] forState:UIControlStateNormal];

        
        if ([_recTrackOne isEqualToString:@"-1"])
        {
            [_deleteImageT1 setImage:[UIImage imageNamed:@"one_red.png" ]];
            [_deleteImageT1 setHidden:NO ];
//            [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_red.png"] forState:UIControlStateNormal];
            [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_greyoutline.png"] forState:UIControlStateNormal];
            [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
            [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
            
        }
        else if ([_recTrackTwo isEqualToString:@"-1"])
        {
            [_deleteImageT2 setImage:[UIImage imageNamed:@"two_red.png" ]];
            [_deleteImageT2 setHidden:NO ];
//            [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_red.png"] forState:UIControlStateNormal];
            [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
            [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
            
        }
        else if ([_recTrackThree isEqualToString:@"-1"])
        {
            [_deleteImageT3 setImage:[UIImage imageNamed:@"three_red.png" ]];
            [_deleteImageT3 setHidden:NO ];
//            [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_red.png"] forState:UIControlStateNormal];
            [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
            
        }
        else if ([_recTrackFour isEqualToString:@"-1"])
        {
            [_deleteImageT4 setImage:[UIImage imageNamed:@"four_red.png" ]];
            [_deleteImageT4 setHidden:NO ];
//            [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_red.png"] forState:UIControlStateNormal];
        }
        
        stopFlag = 1;
        playFlag = 1;
        
        //[_recordView setHidden:NO];
        //[_bpmView setHidden:YES];
        //[_micView setHidden:NO];
        
        // have to give UISlider max value before SET value else it won't update its value and looks not updating.
        _recSlider.maximumValue = [durationStringUnFormatted floatValue];
        
        [_recorder record];
        
        [self playSelectedRecording];
        
        // sending notification to navigation class for hiding cromotic tuner
        [[NSNotificationCenter defaultCenter] postNotificationName:@"tappedRecordButton" object:nil];

        _playTimer = [NSTimer scheduledTimerWithTimeInterval: 0.01f
                                                      target: self
                                                    selector:@selector(updateMicGain:)
                                                    userInfo: nil repeats:YES];
        [_playTimer fire];
        
        _recordTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f
                                                        target: self
                                                      selector:@selector(onPlayOrRecordTimer:)
                                                      userInfo: nil repeats:YES];
        [_recordTimer fire];
        
        _updateSliderTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                              target:self selector:@selector(updateSlider:) userInfo:nil repeats:YES];
        [_updateSliderTimer fire];
        
        
        
    }else if (stopFlag == 1) {
        
        [self resetPlayButtonWithCell];
        //[_recordView setHidden:YES];
        [self recordingFinished ];
    }
}


- (IBAction)onTapClap1Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag1 == 0) {
        [btn setSelected:YES];
        clapFlag1 = 1;
        if(playFlag == 1)
        {
            //_audioPlayerClap1.currentTime = _recAudioPlayer1.currentTime;
            //[self playSelectedRecording];
            _audioPlayerClap1.volume = _musicPlayer.volume;
        }
    } else {
        [btn setSelected:NO];
        clapFlag1 = 0;
        _audioPlayerClap1.volume = 0;
    }
    // [btn setImage:[UIImage imageNamed:@"Clap_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap2Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag2 == 0) {
        [btn setSelected:YES];
        clapFlag2 = 1;
        if(playFlag == 1)
        {
            //[self playSelectedRecording];
            _audioPlayerClap2.volume = _musicPlayer.volume;
        }
    } else {
        [btn setSelected:NO];
        clapFlag2 = 0;
        _audioPlayerClap2.volume = 0;
        
    }
    // [btn setImage:[UIImage imageNamed:@"Claps2_Selected.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap3Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag3 == 0) {
        [btn setSelected:YES];
        clapFlag3 = 1;
        if(playFlag == 1)
        {
            //[self playSelectedRecording];
            _audioPlayerClap3.volume = _musicPlayer.volume;
        }
    } else {
        [btn setSelected:NO];
        clapFlag3 = 0;
        _audioPlayerClap3.volume = 0;
        
    }
    //  [btn setImage:[UIImage imageNamed:@"Claps3_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap4Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag4 == 0) {
        [btn setSelected:YES];
        if(playFlag == 1)
        {
            //            [self playMemos:_droneType];
            //            _audioPlayerClap4.currentTime = 0;
            //            [_audioPlayerClap4 play];
            _audioPlayerClap4.volume = _musicPlayer.volume;
        }
        
        clapFlag4 = 1;
    } else {
        [btn setSelected:NO];
        clapFlag4 = 0;
        _audioPlayerClap4.volume = 0;
    }
    // [btn setImage:[UIImage imageNamed:@"Claps4_Blue.png"] forState:UIControlStateSelected];
}

#pragma mark - Actionsheet delegate------
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        //[self deleteRecords];
        self.alertController = [RAAlertController alertControllerWithTitle:@"Delete Recording" message:@"Are you sure you want to delete Recording?"
                                                            preferredStyle:RAAlertControllerStyleAlert];
        [_alertController addAction:[RAAlertAction actionWithTitle:@"Delete"
                                                             style:RAAlertActionStyleDefault
                                                           handler:^(RAAlertAction *action) {
                                                               NSLog(@"... delete Clicked");
                                                               [self deleteRecords];
                                                           }]];
        [_alertController addAction:[RAAlertAction actionWithTitle:@"Cancel"
                                                             style:RAAlertActionStyleCancel
                                                           handler:^(RAAlertAction *action) {
                                                               NSLog(@"...cancel clicked");
                                                           }]];
        [_alertController presentInViewController:self animated:YES completion:^{
            NSLog(@"Alert!");
        }];
       
    }
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.placeholder = textField.text;
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (![textField.text isEqualToString:@""]) {
        [sqlManager updateRecordingNameOfRecordID:recordID updatedName:_songNameTxtFld.text];
    }
    else
        textField.text = currentRythmName;
    
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - touch implementation

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_songNameTxtFld resignFirstResponder];
    UITouch *t = [[event allTouches] anyObject];
    
    CGPoint p = [t locationInView:self.view];
    if ( CGRectContainsPoint(self.deleteBGView.frame, p))
    {
        NSLog(@"deleteBGview touched");
        
        [_deleteBGView setHidden:YES];
        
        if (!_deleteImageT1.hidden) {
            [_deleteImageT1 setHidden:YES];
            [self.view sendSubviewToBack:_firstVolumeKnob];
        }
        else if (!_deleteImageT2.hidden) {
            [_deleteImageT2 setHidden:YES];
            [self.view sendSubviewToBack:_secondVolumeKnob];
        }
        else if (!_deleteImageT3.hidden) {
            [_deleteImageT3 setHidden:YES];
            [self.view sendSubviewToBack:_thirdVolumeKnob];
        }
        else if (!_deleteImageT4.hidden) {
            [_deleteImageT4 setHidden:YES];
            [self.view sendSubviewToBack:_fourthVolumeKnob];
        }
      
    }
    
}

#pragma mark - methods

- (void)updateUIDataWithDuration :(NSString *)duration
{
    _TotalTimeLbl.text = [self timeFormatted:[duration intValue]];
    _maxRecDurationLbl.text = [NSString stringWithFormat:@"-%@",[self timeFormatted:[duration intValue]]];
    _recSlider.maximumValue = [durationStringUnFormatted floatValue];
}


- (void)deleteRecords
{
    [sqlManager updateDeleteRecordOfRecordID:recordID];
    [self.delegate expandedCellWillCollapse];
}


- (void)setUIElements
{
    _songNameTxtFld.text = currentRythmName;
    _songDetailLbl.text = songDetail;
    _dateLbl.text = dateOfRecording;
    _TotalTimeLbl.text = songDuration;
    _minRecDurationLbl.text = @"00:00";
    _maxRecDurationLbl.text = [NSString stringWithFormat:@"-%@",songDuration];
    
    [_recSlider setThumbImage:[UIImage imageNamed:@"volumesqaure_pointer_blue.png"] forState:UIControlStateNormal];
    [_recSlider setMaximumTrackImage:[UIImage imageNamed:@"volumeslider_sqaure_gred.png"] forState:UIControlStateNormal];
    [_recSlider setMinimumTrackImage:[UIImage imageNamed:@"volumeslider_sqaure_blue.png"] forState:UIControlStateNormal];
    
    _recSlider.minimumValue = 0.00;
    _recSlider.maximumValue = 0.00;
    _recSlider.continuous = YES;
    
    
    int img1 = 1, img2 = 1;
    // Set Image
    if (![rhythmRecord.rhythmInstOneImage isEqualToString:@"-1"]) {
        [_instrument1 setHidden:NO];
        [_volImageInstru1 setHidden:NO];
        [_instrument1 setImage:[UIImage imageNamed:rhythmRecord.rhythmInstOneImage] forState:UIControlStateNormal];
        [_instrument1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",rhythmRecord.rhythmInstOneImage]] forState:UIControlStateSelected];
        
        img1 = 1;
    }else{
        [_instrument1 setHidden:YES];
        [_volImageInstru1 setHidden:YES];
        img1 = 0;
    }
    if (![rhythmRecord.rhythmInstTwoImage isEqualToString:@"-1"]) {
        [_instrument2 setHidden:NO];
        [_volImageInstru2 setHidden:NO];
        [_instrument2 setImage:[UIImage imageNamed:rhythmRecord.rhythmInstTwoImage] forState:UIControlStateNormal];
        [_instrument2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",rhythmRecord.rhythmInstTwoImage]] forState:UIControlStateSelected];
        
        img2 = 1;
    }else{
        [_instrument2 setHidden:YES];
        [_volImageInstru2 setHidden:YES];
        img2 = 0;
    }
    
    CGRect visibleSize = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = visibleSize.size.width;
    //    CGFloat screenHeight = visibleSize.size.height;
    int xDist = 0;  // for 320
    // If only 2 buttons are there
    if ((img1 == 0) && (img2 == 0)) {
        xDist = ((screenWidth - 120) / 3);
        _audioPlayerClap1 = nil;
        _audioPlayerClap2 = nil;
        _instrument3.frame = CGRectMake(xDist, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake(xDist, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*2)+60, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;
        //_dronePickerBackView.frame = CGRectMake((xDist*2)+60, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument3_Layout.constant = xDist;
        //            _Bpm_Layout.constant =  xDist;
        //            _Intrument4_Layout.constant = xDist*2 +60;
        //            _drone_Layout.constant =  xDist*2 +60;
        
        //[self.view setNeedsUpdateConstraints];
        
    }else if (img1 == 0){
        xDist = ((screenWidth - 180) / 4);
        _audioPlayerClap1 = nil;
        _instrument2.frame = CGRectMake(xDist, _instrument2.frame.origin.y, 60, 60);
        _instrument3.frame = CGRectMake((xDist*2)+60, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake((xDist*2)+60, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*3)+120, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru2.center = _instrument2.center;
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;
        //_dronePickerBackView.frame = CGRectMake((xDist*3)+120, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument2_Layout.constant = xDist;
        //            _Instrument3_Layout.constant = xDist*2 +60;
        //            _Bpm_Layout.constant =  xDist*2 +60;
        //            _Intrument4_Layout.constant = xDist*3 +120;
        //            _drone_Layout.constant =  xDist*3 +120;
    }else if (img2 == 0){
        xDist = ((screenWidth - 180) / 4);
        _audioPlayerClap2 = nil;
        _instrument1.frame = CGRectMake(xDist, _instrument1.frame.origin.y, 60, 60);
        _instrument3.frame = CGRectMake((xDist*2)+60, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake((xDist*2)+60, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*3)+120, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru1.center = _instrument1.center;
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;
        //_dronePickerBackView.frame = CGRectMake((xDist*3)+120, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument1_Layout.constant = xDist;
        //            _Instrument3_Layout.constant = xDist*2 +60;
        //            _Bpm_Layout.constant =  xDist*2 +60;
        //            _Intrument4_Layout.constant = xDist*3 +120;
        //            _drone_Layout.constant =  xDist*3 +120;
        
    }else{
        xDist = ((screenWidth - 240) / 5);
        _instrument1.frame = CGRectMake(xDist, _instrument1.frame.origin.y, 60, 60);
        _instrument2.frame = CGRectMake((xDist*2)+60, _instrument2.frame.origin.y, 60, 60);
        _instrument3.frame = CGRectMake((xDist*3)+120, _instrument3.frame.origin.y, 60, 60);
        //_bpmPickderBackView.frame = CGRectMake((xDist*3)+120, _bpmPickderBackView.frame.origin.y, 60, 60);
        _instrument4.frame = CGRectMake((xDist*4)+180, _instrument4.frame.origin.y, 60, 60);
        _volImageInstru1.center = _instrument1.center;
        _volImageInstru2.center = _instrument2.center;
        _volImageInstru3.center = _instrument3.center;
        _volImageInstru4.center = _instrument4.center;        //_dronePickerBackView.frame = CGRectMake((xDist*4)+180, _dronePickerBackView.frame.origin.y, 60, 60);
        
        //            _Instrument1_Layout.constant = xDist;
        //            _Instrument2_Layout.constant = xDist*2 +60;
        //            _Instrument3_Layout.constant = xDist*3 +120;
        //            _Bpm_Layout.constant =  xDist*3 +120;
        //            _Intrument4_Layout.constant = xDist*4 +180;
        //            _drone_Layout.constant =  xDist*4 +180;
    }
    
    
    if ([_recTrackOne isEqualToString:@"-1"])
    {
        [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_redoutline.png"] forState:UIControlStateNormal];
        [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_greyoutline.png"] forState:UIControlStateNormal];
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
    }
    else if ([_recTrackTwo isEqualToString:@"-1"])
    {
        [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_redoutline.png"] forState:UIControlStateNormal];
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
    }
    else if ([_recTrackThree isEqualToString:@"-1"])
    {
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_redoutline.png"] forState:UIControlStateNormal];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
    }
    else if ([_recTrackFour isEqualToString:@"-1"])
    {
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
    }
    
    if (![_recTrackOne isEqualToString:@"-1"])
        [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_blue.png"] forState:UIControlStateSelected];
    if (![_recTrackTwo isEqualToString:@"-1"])
        [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_blue.png"] forState:UIControlStateSelected];
    if (![_recTrackThree isEqualToString:@"-1"])
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_blue.png"] forState:UIControlStateSelected];
    if (![_recTrackFour isEqualToString:@"-1"])
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_blue.png"] forState:UIControlStateSelected];
    
    _maxRecDurationLbl.text = [NSString stringWithFormat:@"-%@",songDuration];
    
    if (![_droneType isEqualToString:@"-1"])
    {
        [_instrument4 setTitle:_droneType forState:UIControlStateNormal];
        [_instrument4 setTitle:_droneType forState:UIControlStateSelected];
    }
    
    if(clapFlag1 == 1)
    {
        _instrument1.selected = YES;
    }
    else
        _instrument1.selected = NO;
    
    if(clapFlag2 == 1)
    {
        _instrument2.selected = YES;
    }
    else
        _instrument2.selected = NO;
    
    if(clapFlag3 == 1)
    {
        _instrument3.selected = YES;
    }
    else
        _instrument3.selected = NO;
    
    if(clapFlag4 == 1)
    {
        _instrument4.selected = YES;
    }
    else
        _instrument4.selected = NO;
    
    if(recFlag1 == 1)
    {
        _firstVolumeKnob.selected = YES;
    }
    else
        _firstVolumeKnob.selected = NO;
    
    if(recFlag2 == 1)
    {
        _secondVolumeKnob.selected = YES;
    }
    else
        _secondVolumeKnob.selected = NO;
    
    if(recFlag3 == 1)
    {
        _thirdVolumeKnob.selected = YES;
    }
    else
        _thirdVolumeKnob.selected = NO;
    
    if(recFlag4 == 1)
    {
        _fourthVolumeKnob.selected = YES;
    }
    else
        _fourthVolumeKnob.selected = NO;
    
}

-(void)setDataForUIElements:(int)_index RecordingData :(RecordingListData *)data
{
    RecordingListData *cellData = [[RecordingListData alloc] init];
    rhythmRecord = [[RhythmClass alloc] init];
    currentIndex = _index; // not used instead of it record id is used
    cellData = data;
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    dataArray = [sqlManager fetchRhythmRecordsByID:[NSNumber numberWithInt:[cellData.rhythmID intValue]]];
    rhythmRecord = [dataArray objectAtIndex:0];
    
    // set instrument ON/OFF
    clapFlag1 = [cellData.instOne intValue];
    clapFlag2 = [cellData.instTwo intValue];
    clapFlag3 = [cellData.instThree intValue];
    clapFlag4 = [cellData.instFour intValue];
    
    // Set Music
    beatOneMusicFile = cellData.beat1;
    beatTwoMusicFile = cellData.beat2;
    _recTrackOne = cellData.trackOne;
    _recTrackTwo = cellData.trackTwo;
    _recTrackThree = cellData.trackThree;
    _recTrackFour = cellData.trackFour;
    _startBPM = cellData.BPM;
    originalBPM = rhythmRecord.rhythmBPM;
    _droneType = cellData.droneType;
    durationStringUnFormatted = cellData.durationString;
    
    t1Duration = cellData.t1DurationString;
    t2Duration = cellData.t2DurationString;
    t3Duration = cellData.t3DurationString;
    t4Duration = cellData.t4DurationString;
    
    [recordingDurationArray addObject:[NSNumber numberWithFloat:[t1Duration floatValue]]];
    [recordingDurationArray addObject:[NSNumber numberWithFloat: [t2Duration floatValue]]];
    [recordingDurationArray addObject:[NSNumber numberWithFloat:[t3Duration floatValue]]];
    [recordingDurationArray addObject:[NSNumber numberWithFloat:[t4Duration floatValue]]];
    
    
    recordID = cellData.recordID;
    currentRythmName = cellData.recordingName;
    songDuration = [NSString stringWithFormat:@"%@",[self timeFormatted:[durationStringUnFormatted floatValue]]];
    dateOfRecording = cellData.dateString;
    songDetail = [NSString stringWithFormat:@"%@ %@ bpm %@",rhythmRecord.rhythmName,cellData.BPM,cellData.droneType];
    
    firstKnob = _firstVolumeKnob;
    secondKnob = _secondVolumeKnob;
    thirdKnob = _thirdVolumeKnob;
    forthKnob = _fourthVolumeKnob;
    
    firstKnobCentre = firstKnob.center;
    secondKnobCentre = secondKnob.center;
    thirdKnobCentre = thirdKnob.center;
    forthKnobCentre = forthKnob.center;
    
    // set recording knob on/off
    if ([_recTrackOne isEqualToString:@"-1"] ) {
        recFlag1 = 0;
    }
    else
        recFlag1 = 1;
    
    if ([_recTrackTwo isEqualToString:@"-1"] ) {
        recFlag2 = 0;
    }
    else
        recFlag2 = 1;
    
    if ([_recTrackThree isEqualToString:@"-1"] ) {
        recFlag3 = 0;
    }
    else
        recFlag3 = 1;
    
    if ([_recTrackFour isEqualToString:@"-1"] ) {
        recFlag4 = 0;
    }
    else
        recFlag4 = 1;
    
}


- (void) swipedScreen:(UISwipeGestureRecognizer*)swipeGestureEffect {
    // do stuff
    UIButton *button = (UIButton *)swipeGestureEffect.view;
    if(swipeGestureEffect.direction == UISwipeGestureRecognizerDirectionUp) {
        NSLog(@"Swipe Up");
        [self rotateImageViewClockWiseWithButtonTag:(int)button.tag];
        
    } else if (swipeGestureEffect.direction == UISwipeGestureRecognizerDirectionDown) {
        NSLog(@"Swipe Down");
        [self rotateImageViewAnticlockWiseWithButtonTag:(int)button.tag];
    }
    
}

- (void)rotateImageViewClockWiseWithButtonTag :(int)tag
{
    
    UIImageView *rotateImage = [[UIImageView alloc] init];
    switch (tag) {
        case 1:
            rotateImage = _volImageInstru1;
            break;
        case 2:
            rotateImage = _volImageInstru2;
            break;
        case 3:
            rotateImage = _volImageInstru3;
            break;
        case 4:
            rotateImage = _volImageInstru4;
            break;
        case 11:
            rotateImage = _volImageT1;
            break;
        case 22:
            rotateImage = _volImageT2;
            break;
        case 33:
            rotateImage = _volImageT3;
            break;
        case 44:
            rotateImage = _volImageT4;
            break;
            
        default:
            break;
    }
    
    if (VolumeKnobLevelCount <= 8) {
        
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [rotateImage setTransform:CGAffineTransformRotate(rotateImage.transform, (360/ 11))];
            //_rotateImage.bounds = CGRectMake( 0, 0, w, h );
        }completion:^(BOOL finished){
            if (finished) {
                //[self rotateImageView];
                VolumeKnobLevelCount ++;
                //_rotateImage.center = _rotateImageCentre;
                
            }
        }];
    }
    else
        return;
}

- (void)rotateImageViewAnticlockWiseWithButtonTag:(int)tag
{
    
    UIImageView *rotateImage = [[UIImageView alloc] init];
    switch (tag) {
        case 1:
            rotateImage = _volImageInstru1;
            break;
        case 2:
            rotateImage = _volImageInstru2;
            break;
        case 3:
            rotateImage = _volImageInstru3;
            break;
        case 4:
            rotateImage = _volImageInstru4;
            break;
        case 11:
            rotateImage = _volImageT1;
            break;
        case 22:
            rotateImage = _volImageT2;
            break;
        case 33:
            rotateImage = _volImageT3;
            break;
        case 44:
            rotateImage = _volImageT4;
            break;
            
        default:
            break;
    }
    
    if (VolumeKnobLevelCount >= 1) {
        
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            [rotateImage setTransform:CGAffineTransformRotate(rotateImage.transform, -(360/ 11))];
        }completion:^(BOOL finished){
            if (finished) {
                //[self rotateImageView];
                VolumeKnobLevelCount --;
                
                
            }
        }];
    }
    else
        return;
    
}


- (void)resetPlayButtonWithCell
{
    [self stopSelectedRhythms];
    playFlag = 0;
    stopFlag = 0;
    [self calculateMicGain:0];
    _recSlider.value = 0.00;
    
    [_recSlider setThumbImage:[UIImage imageNamed:@"volumesqaure_pointer_blue.png"] forState:UIControlStateNormal];
    [_recSlider setMaximumTrackImage:[UIImage imageNamed:@"volumeslider_sqaure_gred.png"] forState:UIControlStateNormal];
    [_recSlider setMinimumTrackImage:[UIImage imageNamed:@"volumeslider_sqaure_blue.png"] forState:UIControlStateNormal];
    
    [_playRecBtn setBackgroundImage:[UIImage imageNamed:@"play-button.png"] forState:UIControlStateNormal];
    [_recordingBtn setEnabled:YES];
    [_playRecBtn setEnabled:YES];
    [_recordingBtn setSelected:NO];
    [_playRecBtn setSelected:NO];
    _minRecDurationLbl.text = @"00:00";
    _maxRecDurationLbl.text = [NSString stringWithFormat:@"-%@",[self timeFormatted:[durationStringUnFormatted intValue]]];
    _recordingTimeLabel.text = @"00:00";
    
    [_recordingBGView setHidden:YES];
    [_deleteBGView setHidden:YES];
    [_deleteImageT1 setHidden:YES ];
    [_deleteImageT2 setHidden:YES ];
    [_deleteImageT3 setHidden:YES ];
    [_deleteImageT4 setHidden:YES ];
    
    [_deleteImageT1 setImage:[UIImage imageNamed:@"closebutton.png"]];
    [_deleteImageT2 setImage:[UIImage imageNamed:@"closebutton.png"]];
    [_deleteImageT3 setImage:[UIImage imageNamed:@"closebutton.png"]];
    [_deleteImageT4 setImage:[UIImage imageNamed:@"closebutton.png"]];
    
    if ([_recTrackOne isEqualToString:@"-1"])
    {
        [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_redoutline.png"] forState:UIControlStateNormal];
        
    }
    else if ([_recTrackTwo isEqualToString:@"-1"])
    {
        [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_redoutline.png"] forState:UIControlStateNormal];
        
    }
    else if ([_recTrackThree isEqualToString:@"-1"])
    {
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_redoutline.png"] forState:UIControlStateNormal];
        
    }
    else if ([_recTrackFour isEqualToString:@"-1"])
    {
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
    }
    
}

- (void) updateMicGain:(NSTimer *)timer {
    
    [_recorder updateMeters];
    
    peakPowerForChannel = pow(10, (0.05 * ([_recorder peakPowerForChannel:0] + [_recorder averagePowerForChannel:0])/2));
    
    endresult = (peakPowerForChannel*100.0f);
    
    if(endresult > 0 && endresult <= 9)
    {
        [self calculateMicGain:0];
    }
    else if(endresult > 9 && endresult <= 10)
    {
        [self calculateMicGain:1];
    }
    else if(endresult > 10 && endresult <= 100)
    {
        [self calculateMicGain:endresult/10 + 1];
    }
    
    
}

- (void) calculateMicGain:(int) gain
{
    
    if(gain == 0)
    {
        for(micCounter = 0;micCounter < 10;micCounter++)
        {
            UIImageView *img = (UIImageView*)[micArray objectAtIndex:micCounter];
            [img setImage:[UIImage imageNamed:@"grey_strips.png"]];
            //micarray[micCounter]->setColor(Color3B(51, 51, 51));
        }
    }
    else if(gain > 0)
    {
        for(micCounter = 1;micCounter <= gain;micCounter++)
        {
            if(micCounter <= 7)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                //    NSLog(@"tag 1-7= %d",img.tag);
                [img setImage:[UIImage imageNamed:@"green-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::GREEN);
            }
            else if(micCounter == 8 || micCounter == 9)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                //   NSLog(@"tag 8-9= %d",img.tag);
                [img setImage:[UIImage imageNamed:@"orange-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::ORANGE);
            }
            else if(micCounter == 10)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                //   NSLog(@"tag 10= %d",img.tag);
                [img setImage:[UIImage imageNamed:@"red-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::RED);
            }
        }
        for(micCounter = gain+1;micCounter < 11;micCounter++)
        {
            UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
            [img setImage:[UIImage imageNamed:@"grey_strips.png"]];
            //micarray[micCounter-1]->setColor(Color3B(51, 51, 51));
        }
    }
}


-(BOOL)renameFileName:(NSString*)oldname withNewName:(NSString*)newname{
    documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                       NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *oldPath = [documentDir stringByAppendingPathComponent:oldname];
    newPath = [documentDir stringByAppendingPathComponent:newname];
    
    
    
    
    
    NSFileManager *fileMan = [NSFileManager defaultManager];
    NSError *error = nil;
    
    if (![fileMan moveItemAtPath:oldPath toPath:newPath error:&error])
    {
        NSLog(@"Failed to move '%@' to '%@': %@", oldPath, newPath, [error localizedDescription]);
        return false;
    }
    
    return true;
}

- (NSString *) timeStamp {
    return [NSString stringWithFormat:@"%ld",(long int)[[NSDate date] timeIntervalSince1970]];
}

-(void)recordingFinished
{
    // Get the input text
    //[self enableDropDownLbl:YES];
    //    currentMusicFileName = [alertView textFieldAtIndex:0].text;
    int value = [[userDefaults objectForKey:@"recodingid"] intValue];
    
    currentMusicFileName = [NSString stringWithFormat:@"music %d",++value];
    
    [userDefaults setObject:[NSString stringWithFormat:@"%d",value] forKey:@"recodingid"];
    [userDefaults synchronize];
    
    //tempoCounter = NSString(audioPlayer.rate;
    time_t     now = time(0);
    struct tm  tstruct;
    char       date[80];
    char       time[80];
    tstruct = *localtime(&now);
    strftime(date, sizeof(date), "%d/%m/%Y", &tstruct);
    strftime(time, sizeof(time), "%X", &tstruct);
    
    currentMusicFileName=[currentMusicFileName stringByAppendingString:@".m4a"];
    NSString *str = [NSString stringWithFormat:@"music_%d_%@.m4a",value,[self timeStamp]];
    [self renameFileName:@"MyAudioMemo.m4a" withNewName:str];
    
    AVAudioPlayer * sound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:newPath] error:nil];
    NSLog(@"Duration = %f",sound.duration);
    
    NSString *songDurationToSave;
    
    if (sound.duration >= [durationStringUnFormatted floatValue]) {
        songDurationToSave = [NSString stringWithFormat:@"%f",sound.duration];
        _TotalTimeLbl.text = [self timeFormatted:[songDurationToSave intValue]];
        durationStringUnFormatted = songDurationToSave;
        _maxRecDurationLbl.text = [NSString stringWithFormat:@"-%@",[self timeFormatted:[songDurationToSave floatValue]] ];
    }
    else
    {
        songDurationToSave = durationStringUnFormatted;
    }
    
    NSDictionary *musicDict = [[NSDictionary alloc]init];
    
    NSLog(@"dict = %@",musicDict);
    
    
    
    if ([_recTrackOne isEqualToString:@"-1"])
    {
        NSLog(@"duration 1 = %@",songDurationToSave);
        [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue] trackSequence:1 track:newPath maxTrackDuration:songDurationToSave trackDuration:[NSString stringWithFormat:@"%f",sound.duration]];
        
        _recTrackOne = newPath;
        recFlag1 = 1;
        t1Duration = [NSString stringWithFormat:@"%f",sound.duration];
        [recordingDurationArray replaceObjectAtIndex:0 withObject:[NSNumber numberWithFloat:[t1Duration floatValue]]];
        [_firstVolumeKnob setSelected:YES];
        [_firstVolumeKnob setBackgroundImage:[UIImage imageNamed:@"one_darkgrey.png"] forState:UIControlStateNormal];
        [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_redoutline.png"] forState:UIControlStateNormal];
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_greyoutline.png"] forState:UIControlStateNormal];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
    }
    else if ([_recTrackTwo isEqualToString:@"-1"])
    {
         NSLog(@"duration 2 = %@",songDurationToSave);
        [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue]  trackSequence:2 track:newPath maxTrackDuration:songDurationToSave trackDuration:[NSString stringWithFormat:@"%f",sound.duration]];
        _recTrackTwo = newPath;
        recFlag2 = 1;
        t2Duration = [NSString stringWithFormat:@"%f",sound.duration];
        [recordingDurationArray replaceObjectAtIndex:1 withObject:[NSNumber numberWithFloat:[t2Duration floatValue]]];
        //[_secondVolumeKnob setHighlighted:NO];
        [_secondVolumeKnob setSelected:YES];
        [_secondVolumeKnob setBackgroundImage:[UIImage imageNamed:@"two_darkgrey.png"] forState:UIControlStateNormal];
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_redoutline.png"] forState:UIControlStateNormal];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_greyoutline.png"] forState:UIControlStateNormal];
    }
    else if ([_recTrackThree isEqualToString:@"-1"])
    {
        NSLog(@"duration 3 = %@",songDurationToSave);
        t3Duration = [NSString stringWithFormat:@"%f",sound.duration];
        [recordingDurationArray replaceObjectAtIndex:2 withObject:[NSNumber numberWithFloat:[t3Duration floatValue]]];
        [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue]  trackSequence:3 track:newPath maxTrackDuration:songDurationToSave trackDuration:[NSString stringWithFormat:@"%f",sound.duration]];
        _recTrackThree = newPath;
        recFlag3 = 1;
        [_thirdVolumeKnob setSelected:YES];
        [_thirdVolumeKnob setBackgroundImage:[UIImage imageNamed:@"three_darkgrey.png"] forState:UIControlStateNormal];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_redoutline.png"] forState:UIControlStateNormal];
        
    }
    else if ([_recTrackFour isEqualToString:@"-1"])
    {
        NSLog(@"duration 4 = %@",songDurationToSave);
        [sqlManager updateSingleRecordingDataWithRecordingId:[recordID intValue]  trackSequence:4 track:newPath maxTrackDuration:songDurationToSave trackDuration:[NSString stringWithFormat:@"%f",sound.duration]];
        _recTrackFour = newPath;
        recFlag4 = 1;
        t4Duration = [NSString stringWithFormat:@"%f",sound.duration];
        [recordingDurationArray replaceObjectAtIndex:3 withObject:[NSNumber numberWithFloat:[t4Duration floatValue]]];
        [_fourthVolumeKnob setSelected:YES];
        [_fourthVolumeKnob setBackgroundImage:[UIImage imageNamed:@"four_darkgrey.png"] forState:UIControlStateNormal];
    }
    
    
}

-(void)onPlayOrRecordTimer:(NSTimer *)timerNotification {
    
    //    seconds++;
    //    if(seconds == 60)
    //    {
    //        seconds = 0;
    //        minutes++;
    //    }
    
    //NSString* duration = [NSString stringWithFormat:@"%.2d:%.2d", minutes, seconds];
    NSString* duration;
    if ([_recorder isRecording]) {
        duration = [self timeFormatted:_recorder.currentTime];
    } else
    {
        
        if((int)_recAudioPlayer1.duration == [durationStringUnFormatted intValue])
            duration = [self timeFormatted:_recAudioPlayer1.currentTime];
        else if ((int)_recAudioPlayer2.duration == [durationStringUnFormatted intValue])
            duration = [self timeFormatted:_recAudioPlayer2.currentTime];
        else if ((int)_recAudioPlayer3.duration == [durationStringUnFormatted intValue])
            duration = [self timeFormatted:_recAudioPlayer3.currentTime];
        else if ((int)_recAudioPlayer4.duration == [durationStringUnFormatted intValue])
            duration = [self timeFormatted:_recAudioPlayer4.currentTime];
    }
    
    // record timer value change
    _recordTimerText = duration;
    _recordingTimeLabel.text = _recordTimerText;
    [self setVolumeInputOutput];
}

-(void)setVolumeInputOutput{
    currentOutputs = _session.currentRoute.outputs;
    for( _output in currentOutputs )
    {
        if([_output.portType isEqualToString:AVAudioSessionPortHeadphones])
        {
            //cout << "                      AVAudioSessionPortHeadphones                          ";
            [_session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
            [_session setPreferredInput:_myPort error:nil];
            break;
        }
        else if([_output.portType isEqualToString:AVAudioSessionPortBuiltInSpeaker])
        {
            //cout << "                      AVAudioSessionPortBuiltInSpeaker                          ";
            break;
        }
        else if([_output.portType isEqualToString:AVAudioSessionPortBuiltInReceiver])
        {
            //cout << "                      AVAudioSessionPortBuiltInReceiver                          ";
            [_session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
            break;
        }
    }
}


- (void) stopSelectedRhythms
{
    if (_clap3Timer != nil) {
        [_clap3Timer invalidate];
        _clap3Timer = nil;
        
        _audioPlayerClap3.currentTime = 0;
        [_audioPlayerClap3 stop];
        _audioPlayerClap3 = nil;
    }
    if (_updateSliderTimer != nil) {
        [_updateSliderTimer invalidate];
        _updateSliderTimer = nil;
    }
    
    if (_playTimer != nil) {
        [_playTimer invalidate];
        _playTimer = nil;
    }
    
    if (_recordTimer != nil) {
        [_recordTimer invalidate];
        _recordTimer = nil;
        seconds = 0;
        minutes = 0;
    }
    
    for (AVAudioPlayer *player in audioPlayerArray) {
        [player stop];
        //[audioPlayerArray removeObject:player];
    }
    [audioPlayerArray removeAllObjects];
    
    _audioPlayerClap1 = nil;
    _audioPlayerClap2 = nil;
    _audioPlayerClap3 = nil;
    _audioPlayerClap4 = nil;
    _recAudioPlayer1 = nil;
    
    if ([_recorder isRecording]) {
        [_recorder stop];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recordingDone" object:nil];
    }
    
}

-(void)clap3timerMethod {
    if (clapFlag3 == 1 &&( stopFlag ==1 || playFlag == 1)) {
        [_audioPlayerClap3 prepareToPlay];
        [_audioPlayerClap3 play];
    }
}

-(void)updateSlider:(NSTimer *)timer {
    
    // Update the slider about the music time
    
    if ([_recorder isRecording]) {
        NSString *maxDuration = [self timeFormatted:_recorder.currentTime];
        [_recorder updateMeters];
        _minRecDurationLbl.text = @"00:00";
        _maxRecDurationLbl.text = maxDuration;
        [_recSlider setValue:_recorder.currentTime animated:YES];
    }
    else
    {
        if((int)_recAudioPlayer1.duration == [durationStringUnFormatted intValue])
        {
            NSString *minDuration = [self timeFormatted:_recAudioPlayer1.currentTime];
        
        NSString *maxDuration = [self timeFormatted:_recAudioPlayer1.duration - _recAudioPlayer1.currentTime];
        
        _minRecDurationLbl.text = minDuration;
        _maxRecDurationLbl.text =  [NSString stringWithFormat:@"-%@",maxDuration];
        _recSlider.value = _recAudioPlayer1.currentTime;
        }
        else if ((int)_recAudioPlayer2.duration == [durationStringUnFormatted intValue])
        {
            NSString *minDuration = [self timeFormatted:_recAudioPlayer2.currentTime];
            
            NSString *maxDuration = [self timeFormatted:_recAudioPlayer2.duration - _recAudioPlayer2.currentTime];
            
            _minRecDurationLbl.text = minDuration;
            _maxRecDurationLbl.text =  [NSString stringWithFormat:@"-%@",maxDuration];
            _recSlider.value = _recAudioPlayer2.currentTime;
        }
        else if ((int)_recAudioPlayer3.duration == [durationStringUnFormatted intValue])
        {
            NSString *minDuration = [self timeFormatted:_recAudioPlayer3.currentTime];
            
            NSString *maxDuration = [self timeFormatted:_recAudioPlayer3.duration - _recAudioPlayer3.currentTime];
            
            _minRecDurationLbl.text = minDuration;
            _maxRecDurationLbl.text =  [NSString stringWithFormat:@"-%@",maxDuration];
            _recSlider.value = _recAudioPlayer3.currentTime;
        }
        else if ((int)_recAudioPlayer4.duration == [durationStringUnFormatted intValue])
        {
            NSString *minDuration = [self timeFormatted:_recAudioPlayer4.currentTime];
            
            NSString *maxDuration = [self timeFormatted:_recAudioPlayer4.duration - _recAudioPlayer4.currentTime];
            
            _minRecDurationLbl.text = minDuration;
            _maxRecDurationLbl.text =  [NSString stringWithFormat:@"-%@",maxDuration];
            _recSlider.value = _recAudioPlayer4.currentTime;
        }
        
    }
    
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int second = totalSeconds % 60;
    int minute = (totalSeconds / 60) % 60;
    //int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d",minute, second];
}

- (void)playMemos:(NSString *)memo
{
    _audioPlayerClap4 = nil;
    
    _audioPlayerClap4 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.m4a", [[NSBundle mainBundle] resourcePath], memo]] error:nil];
    _audioPlayerClap4.volume = 1.0f;
    _audioPlayerClap4.numberOfLoops = -1;
}


- (void) playSelectedRecording
{
    
    NSLog(@"track1 play = %@",_recTrackOne);
    //NSLog(@"Beat2 play = %@",beatTwoMusicFile);
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    
    NSArray *listItems = [beatOneMusicFile componentsSeparatedByString:@"/"];
    NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];
    
    NSError *playerError1;
    
    _audioPlayerClap1 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString]] error:&playerError1];
    
    
    //[_audioPlayerClap1 setDelegate:self];
    //[_audioPlayerClap1 prepareToPlay];
    
    if(_audioPlayerClap1 != nil)
    {
        [audioPlayerArray addObject:_audioPlayerClap1];
        [_audioPlayerClap1 prepareToPlay];
    }
    
    //[self playRecordingWithBeats:_audioPlayerClap1 Beat:0];
    
    
    listItems = [beatTwoMusicFile componentsSeparatedByString:@"/"];
    lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];
    NSError *playerError2;
    
    _audioPlayerClap2 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString ]] error:&playerError2];
    
    if(_audioPlayerClap2 != nil)
    {
        [audioPlayerArray addObject:_audioPlayerClap2];
        [_audioPlayerClap2 prepareToPlay];
    }
    
    
    NSError *playerError3;
    _audioPlayerClap3 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Min_Click_60bpm.m4a", [[NSBundle mainBundle] resourcePath]]] error:&playerError3];
    
    if(_audioPlayerClap3 != nil)
    {
        [audioPlayerArray addObject:_audioPlayerClap3];
        [_audioPlayerClap3 prepareToPlay];
    }
    
    
    [self playMemos:_droneType];
    if(_audioPlayerClap4 != nil)
    {
        [audioPlayerArray addObject:_audioPlayerClap4];
        [_audioPlayerClap4 prepareToPlay];
    }
    
    if(clapFlag1 == 0 && clapFlag2 == 0 && clapFlag3 == 0) {
        //[self playSongCall:1];
        
    }
    
    NSError *playerErrorR1;
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[_recTrackOne lastPathComponent]];
    
    _recAudioPlayer1 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:filePath] error:&playerErrorR1];
    
    if(_recAudioPlayer1 != nil)
    {
        [audioPlayerArray addObject:_recAudioPlayer1];
        [_recAudioPlayer1 prepareToPlay];
    }
    
    
    NSError *playerErrorR2;
    filePath = [documentsDirectory stringByAppendingPathComponent:[_recTrackTwo lastPathComponent]];
    
    _recAudioPlayer2 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:filePath] error:&playerErrorR2];
    if(_recAudioPlayer2 != nil)
    {
        [audioPlayerArray addObject:_recAudioPlayer2];
        [_recAudioPlayer2 prepareToPlay];

    }
    
    
    NSError *playerErrorR3;
    filePath = [documentsDirectory stringByAppendingPathComponent:[_recTrackThree lastPathComponent]];
    _recAudioPlayer3 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:filePath] error:&playerErrorR3];
    if(_recAudioPlayer3 != nil)
    {
        [audioPlayerArray addObject:_recAudioPlayer3];
        [_recAudioPlayer3 prepareToPlay];

    }
    //[_recAudioPlayer3 setDelegate:self];
    
    
    NSError *playerErrorR4;
    
    filePath = [documentsDirectory stringByAppendingPathComponent:[_recTrackFour lastPathComponent]];
    _recAudioPlayer4 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:filePath] error:&playerErrorR4];
    
    if(_recAudioPlayer4 != nil)
    {
        [audioPlayerArray addObject:_recAudioPlayer4];
        [_recAudioPlayer4 prepareToPlay];
        
    }
    
    currentTime = _audioPlayerClap3.deviceCurrentTime + 0.2;
    
    if((int)_recAudioPlayer1.duration == [durationStringUnFormatted intValue])
        [_recAudioPlayer1 setDelegate:self];
    else if ((int)_recAudioPlayer2.duration == [durationStringUnFormatted intValue])
        [_recAudioPlayer2 setDelegate:self];
    else if ((int)_recAudioPlayer3.duration == [durationStringUnFormatted intValue])
        [_recAudioPlayer3 setDelegate:self];
    else if ((int)_recAudioPlayer4.duration == [durationStringUnFormatted intValue])
        [_recAudioPlayer4 setDelegate:self];
    
    [self playRecordingWithBeats:audioPlayerArray];
    
    if(clapFlag1 == 1) {
        _audioPlayerClap1.volume = _musicPlayer.volume;
    }else
        _audioPlayerClap1.volume = 0;
    
    if(clapFlag2 == 1) {
        _audioPlayerClap2.volume = _musicPlayer.volume;
    }
    else
        _audioPlayerClap2.volume = 0;
    
    if(clapFlag3 == 1) {
        _audioPlayerClap3.volume = _musicPlayer.volume;
    }
    else
        _audioPlayerClap3.volume = 0;
    
    if(clapFlag4 == 1) {
        _audioPlayerClap4.volume = _musicPlayer.volume;
    }
    else
        _audioPlayerClap4.volume = 0;
    
    if(recFlag1 == 1)
    {
        _recAudioPlayer1.volume = _musicPlayer.volume;
    }else
        _recAudioPlayer1.volume = 0;
    
    if(recFlag2 == 1)
    {
        _recAudioPlayer2.volume = _musicPlayer.volume;
    }else
        _recAudioPlayer2.volume = 0;
    
    if(recFlag3 == 1)
    {
        _recAudioPlayer3.volume = _musicPlayer.volume;
    }else
        _recAudioPlayer3.volume = 0;
    
    if(recFlag4 == 1)
    {
        _recAudioPlayer4.volume = _musicPlayer.volume;
    }else
        _recAudioPlayer4.volume = 0;
    
}

- (void)clap3timerAction {
    if (clapFlag3 == 1 &&( stopFlag ==1 || playFlag == 1)) {
        //  _audioPlayerClap3.enableRate = true;
        //  _audioPlayerClap3.rate = mCurrentScore/tempo;
        //[_audioPlayerClap3 prepareToPlay];
        [_audioPlayerClap3 play];
        
        //        [self playRhythmsWithBeats:_audioPlayerClap3 Beat:0];
    }
}

- (void) playRecordingWithBeats:(AVAudioPlayer*)audioPlayer Beat:(int)beatNumber
{
    
    audioPlayer.numberOfLoops = 0;
    audioPlayer.enableRate = true;
    //audioPlayer.rate = mCurrentScore/tempo;
    
    [audioPlayer playAtTime:currentTime];
    
}

- (void) playRecordingWithBeats:(NSMutableArray*)playerArray
{
    
    //    NSTimeInterval currentTime = ((AVAudioPlayer *)[playerArray objectAtIndex:0]).deviceCurrentTime;
    
    for (AVAudioPlayer *player in playerArray) {
        if(![player isPlaying])
        {
            if (player == _audioPlayerClap1 || player == _audioPlayerClap2 || player == _audioPlayerClap3 || player == _audioPlayerClap4)
            {
                player.numberOfLoops = -1;
            }
            else
            player.numberOfLoops = 0;
            
            player.enableRate = true;
            
            if (player == _audioPlayerClap3) {
                player.rate = [_startBPM floatValue]/60;
            }
            else
            player.rate = [_startBPM floatValue]/[originalBPM floatValue];
            
            [player playAtTime:currentTime];
        }
        
    }
    
}

- (void) LongPress:(UILongPressGestureRecognizer *)gesture
{
    
    NSLog(@"gsture = %@",gesture);
    NSLog(@"viewtag = %ld",(long)gesture.view.tag);
    UIButton *button = (UIButton *)gesture.view;
    if ((button.tag == 11 && [_recTrackOne isEqualToString:@"-1"]) || (button.tag == 22 && [_recTrackTwo isEqualToString:@"-1"]) || (button.tag == 33 && [_recTrackThree isEqualToString:@"-1"]) || (button.tag == 44 && [_recTrackFour isEqualToString:@"-1"])) {
        return;
    }
    [_deleteBGView setHidden:NO];
    if (button.tag == 11) {
        [_deleteImageT1 setHidden:NO];
        [self.view bringSubviewToFront:_firstVolumeKnob];
        [self.view bringSubviewToFront:_deleteImageT1];
        //[_firstVolumeKnob setHidden:YES];
    }
    else if(button.tag == 22)
    {
        [_deleteImageT2 setHidden:NO];
        [self.view bringSubviewToFront:_secondVolumeKnob];
        [self.view bringSubviewToFront:_deleteImageT2];
    }
    else if(button.tag == 33)
    {
        [_deleteImageT3 setHidden:NO];
        [self.view bringSubviewToFront:_thirdVolumeKnob];
        [self.view bringSubviewToFront:_deleteImageT3];
    }
    else
    {
        [_deleteImageT4 setHidden:NO];
        [self.view bringSubviewToFront:_fourthVolumeKnob];
        [self.view bringSubviewToFront:_deleteImageT4];
    }

    didHold = YES;
    
}



- (IBAction) imageMoved:(id) sender withEvent:(UIEvent *) event
{
    //BOOL insideTrash = NO;
    
    //if (dragEnabled)
    
    UIControl *control = sender ;
    int tag = (int)control.tag;
    NSLog(@"tag = %d",tag);
    UITouch *t = [[event allTouches] anyObject];
    CGPoint pPrev = [t previousLocationInView:control];
    CGPoint p = [t locationInView:control];
    
    CGPoint center = control.center;
    center.x += p.x - pPrev.x;
    center.y += p.y - pPrev.y;
    control.center = center;
    
    
    if(CGRectContainsPoint(control.frame, trashButton.center))
    {
        
        //control.center = forthKnobCentre;
        
        if(t.phase == UITouchPhaseEnded)
        {
            //               control.hidden = YES;
            //               control.center = forthKnobCentre;
            //               secondKnob.center = control.center;
            //               thirdKnob.center = control.center;
            //               forthKnob.center = control.center;
            
            if(tag == 1)
            {
                control.hidden = YES;
                control.center = secondKnobCentre;
                secondKnob.center = thirdKnobCentre;
                thirdKnob.center = forthKnobCentre;
                forthKnob.center = forthKnobCentre;
                
                
                [UIView animateWithDuration:0.5
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     // set the new frame
                                     control.hidden = NO;
                                     control.center = firstKnobCentre;
                                     secondKnob.center = secondKnobCentre;
                                     thirdKnob.center = thirdKnobCentre;
                                     forthKnob.hidden = YES;
                                     //control.center = forthKnobCentre;
                                     
                                 }
                                 completion:^(BOOL finished){
                                     NSLog(@"Done!");
                                     //sleep(1);
                                     forthKnob.hidden = NO;
                                     forthKnob.center = forthKnobCentre;
                                 }
                 ];
            }
            
            if(tag == 2)
            {
                control.hidden = YES;
                control.center = thirdKnobCentre;
                //secondKnob.center = thirdKnobCentre;
                thirdKnob.center = forthKnobCentre;
                forthKnob.center = forthKnobCentre;
                
                
                [UIView animateWithDuration:0.5
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     // set the new frame
                                     control.hidden = NO;
                                     control.center = secondKnobCentre;
                                     //secondKnob.center = secondKnobCentre;
                                     thirdKnob.center = thirdKnobCentre;
                                     forthKnob.hidden = YES;
                                     //control.center = forthKnobCentre;
                                     
                                 }
                                 completion:^(BOOL finished){
                                     NSLog(@"Done!");
                                     //sleep(1);
                                     forthKnob.hidden = NO;
                                     forthKnob.center = forthKnobCentre;
                                 }
                 ];
            }
            
            if(tag == 3)
            {
                control.hidden = YES;
                control.center = forthKnobCentre;
                //secondKnob.center = thirdKnobCentre;
                //thirdKnob.center = forthKnobCentre;
                forthKnob.center = forthKnobCentre;
                
                
                [UIView animateWithDuration:0.5
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     // set the new frame
                                     control.hidden = NO;
                                     control.center = thirdKnobCentre;
                                     //secondKnob.center = secondKnobCentre;
                                     //thirdKnob.center = thirdKnobCentre;
                                     forthKnob.hidden = YES;
                                     //control.center = forthKnobCentre;
                                     
                                 }
                                 completion:^(BOOL finished){
                                     NSLog(@"Done!");
                                     //sleep(1);
                                     forthKnob.hidden = NO;
                                     forthKnob.center = forthKnobCentre;
                                 }
                 ];
            }
            if(tag == 4)
            {
                control.hidden = YES;
                control.center = forthKnobCentre;
                //secondKnob.center = thirdKnobCentre;
                //thirdKnob.center = forthKnobCentre;
                forthKnob.center = forthKnobCentre;
                
                
                [UIView animateWithDuration:0.5
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseOut
                                 animations:^{
                                     // set the new frame
                                     control.hidden = NO;
                                     //control.center = forthKnobCentre;
                                     //secondKnob.center = secondKnobCentre;
                                     //thirdKnob.center = thirdKnobCentre;
                                     //forthKnob.hidden = YES;
                                     //control.center = forthKnobCentre;
                                     
                                 }
                                 completion:^(BOOL finished){
                                     NSLog(@"Done!");
                                     //sleep(1);
                                     //                                        forthKnob.hidden = NO;
                                     //                                        forthKnob.center = forthKnobCentre;
                                 }
                 ];
            }
            
        }
    }
    else
    {
        if(t.phase == UITouchPhaseEnded)
        {
            if (control.tag == 1) {
                control.hidden = NO;
                control.center = firstKnobCentre;
            }
            if (control.tag == 2) {
                control.hidden = NO;
                control.center = secondKnobCentre;
            }
            if (control.tag == 3) {
                control.hidden = NO;
                control.center = thirdKnobCentre;
            }
            if (control.tag == 4) {
                control.hidden = NO;
                control.center = forthKnobCentre;
            }
            
        }
    }
    // dragEnabled = NO;
    
}




@end
