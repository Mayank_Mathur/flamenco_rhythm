//
//  ViewController.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

//#import "AppDelegate.h"
#import "ViewController.h"
#import "RecordViewController.h"
#import "MainNavigationViewController.h"
#import "DBManager.h"
#import "GenreClass.h"

@interface ViewController (){
 //   AppDelegate *appDelgate;
    MainNavigationViewController *navController;
//    NSArray *imageViewArray;
 //   int imageFlag;
    NSMutableArray *genreArray;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
  //  appDelgate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
 //   imageViewArray = [[NSArray alloc]initWithObjects:_imageView1,_imageView2,_imageView3,_imageView4,_imageView5,_imageView6,_imageView7,_imageView8,_imageView8,_imageView9,_imageView10, nil];
 //   imageFlag = -1;
//    genreArray = [[NSMutableArray alloc]init];
    
    [self fetchDBData];
    
}


// Fetching data from Database for the First time
-(void)fetchDBData{
    DBManager *rhythmQuery = [[DBManager alloc]init];
    genreArray = [rhythmQuery getAllGenreDetails];
    
    _rhythmBtn1.tag = [[[genreArray objectAtIndex:0] genreId] integerValue];
    [_rhythmBtn1 setTitle:[[genreArray objectAtIndex:0]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn2.tag = [[[genreArray objectAtIndex:1] genreId] integerValue];
    [_rhythmBtn2 setTitle:[[genreArray objectAtIndex:1]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn3.tag = [[[genreArray objectAtIndex:2] genreId] integerValue];
    [_rhythmBtn3 setTitle:[[genreArray objectAtIndex:2]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn4.tag = [[[genreArray objectAtIndex:3] genreId] integerValue];
    [_rhythmBtn4 setTitle:[[genreArray objectAtIndex:3]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn5.tag = [[[genreArray objectAtIndex:4] genreId] integerValue];
    [_rhythmBtn5 setTitle:[[genreArray objectAtIndex:4]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn6.tag = [[[genreArray objectAtIndex:5] genreId] integerValue];
    [_rhythmBtn6 setTitle:[[genreArray objectAtIndex:5]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn7.tag = [[[genreArray objectAtIndex:6] genreId] integerValue];
    [_rhythmBtn7 setTitle:[[genreArray objectAtIndex:6]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn8.tag = [[[genreArray objectAtIndex:7] genreId] integerValue];
    [_rhythmBtn8 setTitle:[[genreArray objectAtIndex:7]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn9.tag = [[[genreArray objectAtIndex:8] genreId] integerValue];
    [_rhythmBtn9 setTitle:[[genreArray objectAtIndex:8]genreName] forState:UIControlStateNormal];
    
    _rhythmBtn10.tag = [[[genreArray objectAtIndex:9] genreId] integerValue];
    [_rhythmBtn10 setTitle:[[genreArray objectAtIndex:9]genreName] forState:UIControlStateNormal];
    
    
    
}


-(void)viewDidLayoutSubviews{
    //[self.btnScrollView setContentSize:CGSizeMake(self.view.frame.size.width, 568)];
}

- (IBAction)onTapGenreBtn:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    for (int i = 1; i <= 10 ; i++) {
        UIButton *button = (UIButton*)[self.btnsView viewWithTag:i];
        [button setSelected:NO];
    }
    
    if (btn.tag == 1) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 2) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 3) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 4) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 5) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 6) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 7) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 8) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 9) {
        [btn setSelected:YES];
        
    }else if(btn.tag == 10) {
        [btn setSelected:YES];
        
    }

    navController = self.myNavigationController;
    NSLog(@"the nav address: %@",navController);
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%ld",(long)btn.tag],@"genreId",@"1",@"bpmDefault", nil];
    [navController viewToPresent:1 withDictionary:dict];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidAppear:animated];

}

- (void)viewDidUnload
{
    [super viewDidUnload];

}


    
//    _myNavigationController = self.myNavigationController;
//    NSLog(@"the nav address: %@",_myNavigationController);
//    [_myNavigationController viewToPresent:1];

/*


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touch Began");
    NSSet* allTouches = [touches setByAddingObjectsFromSet:[event touchesForView:self.view]];
    
    for (int keyIndex = 0; keyIndex < [imageViewArray count]; keyIndex++) {
        UIImageView* key = imageViewArray[keyIndex];
        //BOOL keyIsPressed = NO;
        for (UITouch* touch in allTouches) {
            CGPoint location = [touch locationInView:self.view];
            if(CGRectContainsPoint(key.frame, location))
            {
                [key setImage:[UIImage imageNamed:@"BlockBlue.png"]];
                //key.backgroundColor = [UIColor blueColor];
                imageFlag = (int)key.tag;
            }
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    NSLog(@"Touch Moved");
    NSSet* allTouches = [touches setByAddingObjectsFromSet:[event touchesForView:self.view]];
    
    for (int keyIndex = 0; keyIndex < [imageViewArray count]; keyIndex++) {
        UIImageView* key = imageViewArray[keyIndex];
        //BOOL keyIsPressed = NO;
        for (UITouch* touch in allTouches) {
            CGPoint location = [touch locationInView:self.view];
            if(CGRectContainsPoint(key.frame, location))
            {
                NSLog(@"Inside btn = %ld",(long)key.tag);
                //  = [UIColor blueColor];
                [key setImage:[UIImage imageNamed:@"BlockBlue.png"]];
             //   key.backgroundColor = [UIColor blueColor];
                imageFlag = 0;
                if (key.tag == imageFlag) {
                    //                    key.backgroundColor = [UIColor blueColor];
                    imageFlag = (int)key.tag;
                }else{
                    //                    UIImageView *img = [btnArray objectAtIndex:(btnFlag-1)];
                    //                    img.backgroundColor = [UIColor grayColor];
                    //                    btnFlag = key.tag;
                }
                
            }
            else if (imageFlag == 0)
            {
              //  key.backgroundColor = [UIColor grayColor];
                [key setImage:[UIImage imageNamed:@"Bar.png"]];
            }
        }
    }
}
//-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//    imageFlag = -1;
//}
*/

@end
