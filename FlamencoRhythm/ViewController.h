//
//  ViewController.h
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MainNavigationViewController;
//#import "CustomScrollView.h"


@interface ViewController : UIViewController<UIScrollViewDelegate>{
    
}
@property (strong, nonatomic) IBOutlet UIView *btnsView;
@property (strong, nonatomic) IBOutlet UIScrollView *btnScrollView;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn1;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn2;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn3;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn4;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn5;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn6;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn7;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn8;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn9;
@property (strong, nonatomic) IBOutlet UIButton *rhythmBtn10;

@property (strong, nonatomic) MainNavigationViewController *myNavigationController;
- (IBAction)onTapGenreBtn:(id)sender;

@end

